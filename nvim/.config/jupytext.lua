-- https://github.com/GCBallesteros/jupytext.nvim

-- Seamlessly open Jupyter Notebooks as there associated
-- plain text alternatives.
-- Powered by jupytext.
-- jupytext.nvim is a lua port of the original jupytext.vim
-- with some additional features and a simpler configuration.

return {
  "GCBallesteros/jupytext.nvim",
  config = true,
  -- Depending on your nvim distro or config you may need to make the loading not lazy
  -- lazy=false,
}
