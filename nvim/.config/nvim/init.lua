--[[
                       _                              __ _
 _ __   ___  _____   _(_)_ __ ___     ___ ___  _ __  / _(_) __ _
| '_ \ / _ \/ _ \ \ / / | '_ ` _ \   / __/ _ \| '_ \| |_| |/ _` |
| | | |  __/ (_) \ V /| | | | | | | | (_| (_) | | | |  _| | (_| |
|_| |_|\___|\___/ \_/ |_|_| |_| |_|  \___\___/|_| |_|_| |_|\__, |
                                                           |___/

--]]

-- HEADER ------------------------------------------------------------------<<<
--[[

=====================================================================
==================== READ THIS BEFORE CONTINUING ====================
=====================================================================
========                                    .-----.          ========
========         .----------------------.   | === |          ========
========         |.-""""""""""""""""""-.|   |-----|          ========
========         ||                    ||   | === |          ========
========         ||   KICKSTART.NVIM   ||   |-----|          ========
========         ||                    ||   | === |          ========
========         ||                    ||   |-----|          ========
========         ||:Tutor              ||   |:::::|          ========
========         |'-..................-'|   |____o|          ========
========         `"")----------------(""`   ___________      ========
========        /::::::::::|  |::::::::::\  \ no mouse \     ========
========       /:::========|  |==hjkl==:::\  \ required \    ========
========      '""""""""""""'  '""""""""""""'  '""""""""""'   ========
========                                                     ========
=====================================================================
=====================================================================

What is Kickstart?

  Kickstart.nvim is *not* a distribution.
    If you don't know anything about Lua, I recommend taking some time to read through
    a guide. One possible example which will only take 10-15 minutes:
      - https://learnxinyminutes.com/docs/lua/

    After understanding a bit more about Lua, you can use `:help lua-guide` as a
    reference for how Neovim integrates Lua.
    - :help lua-guide
    - (or HTML version): https://neovim.io/doc/user/lua-guide.html

Kickstart Guide:
    MOST IMPORTANTLY, we provide a keymap "<space>sh" to [s]earch the [h]elp documentation,
    which is very useful when you're not exactly sure of what you're looking for.

--]]
-- Fin : HEADER ------------------------------------------------------------>>>

-- functions test gui ------------------------------------------------------<<<
local function If_gui()
  if (vim.env.TERM == 'alacritty') or (vim.env.TERM == 'xterm-256color') or (vim.env.TERM == 'xterm-kitty') or (vim.env.TERM == 'foot') then
    return true
  else
    return false
  end
end
-- Fin : functions test gui ------------------------------------------------>>>

-- setting options
require('settings')

-- lazy.nvim plugins manager
-- Make sure to setup `mapleader` and `maplocalleader` before
-- loading lazy.nvim so that mappings are correct.
require("config_lazy")

-- Basic Autocommands
require('autocommands')

-- Configure LSP ----------------------------------------------------------<<<

-- LSP signs in gutter ----------------------------------------------------<<<
if If_gui() then
  vim.diagnostic.config({
      signs = {
          text = {
              [vim.diagnostic.severity.ERROR] = '',
              [vim.diagnostic.severity.WARN] = '',
              [vim.diagnostic.severity.HINT] = '',
              [vim.diagnostic.severity.INFO] = '',
          },
      },
  })
else
  vim.diagnostic.config({
      signs = {
          text = {
              [vim.diagnostic.severity.ERROR] = 'E',
              [vim.diagnostic.severity.WARN] = 'W',
              [vim.diagnostic.severity.HINT] = 'H',
              [vim.diagnostic.severity.INFO] = 'I',
          },
      },
  })
end
-- Fin : LSP signs in gutter ---------------------------------------------->>>

-- Mason
require('lsp.mason')

-- Fin : Configure LSP ---------------------------------------------------->>>

-- nvim-cmp
require('nvim-cmp')

-- keymaps
require('keymaps')

-- theme
if If_gui() then
  vim.o.background = "dark" -- or "light" for light mode
  vim.cmd("colorscheme gruvbox-material")
else
  vim.cmd("colorscheme minischeme")
end

-- The line beneath this is called `modeline`. See `:help modeline`
-- vim: ts=2 sts=2 sw=2 et
