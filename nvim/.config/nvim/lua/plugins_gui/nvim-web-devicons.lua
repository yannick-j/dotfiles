-- https://github.com/nvim-tree/nvim-web-devicons

-- lua `fork` of vim-web-devicons for neovim

return {
  'nvim-tree/nvim-web-devicons',
  opts = {
    -- your personnal icons can go here (to override)
    -- you can specify color or cterm_color instead of specifying both of them
    -- DevIcon will be appended to `name`
    override = {
    ["md"] = {
      icon = "",
      color = "#519aba",
      cterm_color = "74",
      name = "Markdown",
      },
    },
  },
}
