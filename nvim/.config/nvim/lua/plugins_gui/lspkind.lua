-- https://github.com/norcalli/nvim-colorizer.lua
-- A high-performance color highlighter for Neovim
-- which has no external dependencies!
-- Written in performant Luajit.

return {
{
    "norcalli/nvim-colorizer.lua",
    event = "BufReadPre",
    opts = { -- set to setup table
    },
}
}
