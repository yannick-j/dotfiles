-- https://github.com/lukas-reineke/indent-blankline.nvim

-- Add indentation guides even on blank lines

return {
  'lukas-reineke/indent-blankline.nvim',
  -- tag = 'v3.8.2',
  -- enabled = true,
  -- See `:help ibl`
  main = 'ibl',
  ---@module "ibl"
  ---@type ibl.config
  opts = {
    exclude = { filetypes = { "startify", "dashboard" } },
  },
  config = function(_, opts)
    -- paste the hooks code here
    -- change the setup() call to:
    require("ibl").setup(opts)
  end
}
