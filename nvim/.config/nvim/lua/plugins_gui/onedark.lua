-- https://github.com/navarasu/onedark.nvim

return {
  'navarasu/onedark.nvim',
  priority = 1000,
  -- lazy = true,
  config = function()
    require("onedark").setup({
      style = 'warmer',
    })
  end,
}
