-- https://github.com/sainnhe/everforest

-- Everforest is a green based color scheme;
-- it's designed to be warm and soft in order to protect developers' eyes.

return {
  'sainnhe/everforest',
  -- lazy = true,
  config = function()
    vim.cmd([[
      let g:everforest_background = 'hard'
      let g:everforest_better_performance = 1
      let g:everforest_sign_column_background = 'grey'
    ]])
  end
}
