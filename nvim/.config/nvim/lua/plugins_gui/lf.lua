-- lf integration in vim and neovim
-- see :h lf.nvim

return {
  -- 'lmburns/lf.nvim',
  -- dependencies = {
  --   { 'akinsho/toggleterm.nvim', version = "*", config = true },
  -- },
  -- config = function()
  -- require("lf").setup({
  --   border = "rounded", -- border kind: single double shadow curved
  --   escape_quit = true, -- map escape to the quit command (so it doesn't go into a meta normal mode)
  --   winblend = 10, -- pseudotransparency level
  --   height = 40, -- height of the floating window
  --   width = 120, -- width of the floating window
  --   -- Layout configurations
  --   layout_mapping = "<M-u>", -- resize window with this key
  --   views = { -- window dimensions to rotate through
  --     {width = 0.800, height = 0.800},
  --     {width = 0.600, height = 0.600},
  --     {width = 0.950, height = 0.950},
  --     {width = 0.500, height = 0.500, col = 0, row = 0},
  --     {width = 0.500, height = 0.500, col = 0, row = 0.5},
  --     {width = 0.500, height = 0.500, col = 0.5, row = 0},
  --     {width = 0.500, height = 0.500, col = 0.5, row = 0.5},
  --   },
  -- })
  -- end
}
