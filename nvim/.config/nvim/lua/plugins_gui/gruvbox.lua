-- https://github.com/ellisonleao/gruvbox.nvim
-- A port of gruvbox community theme to lua with treesitter and semantic highlights support!

-- https://github.com/morhetz/gruvbox

-- Designed as a bright theme with pastel 'retro groove' colors
-- and light/dark mode switching in the way of solarized.
-- The main focus when developing gruvbox is to keep colors
-- easily distinguishable, contrast enough and still pleasant for the eyes.

return {
  'ellisonleao/gruvbox.nvim',
  priority = 1000,
  config = function()
    ---@diagnostic disable-next-line
    if If_gui() then
      require("gruvbox").setup({
        contrast = "hard", -- can be "hard", "soft" or empty string
        -- transparent_mode = true,
        -- https://github.com/ellisonleao/gruvbox.nvim/pull/355/commits/6745eff5a27a71ce0afe33de5cbfe98dc17b4780
      })
    else
      require("gruvbox").setup({
        terminal_colors = false, -- add neovim terminal colors
        undercurl = false,
        underline = false,
        bold = false,
        italic = {
          strings = false,
          emphasis = false,
          comments = false,
          operators = false,
          folds = false,
        },
        strikethrough = false,
        invert_selection = false,
        invert_signs = false,
        invert_tabline = false,
        invert_intend_guides = false,
        inverse = true, -- invert background for search, diffs, statuslines and errors
        contrast = "soft", -- can be "hard", "soft" or empty string
        palette_overrides = {},
        overrides = {},
        dim_inactive = false,
        transparent_mode = false,
      })
    end
    -- vim.o.background = "dark" -- or "light" for light mode
    -- vim.cmd("colorscheme gruvbox")
  end
}
