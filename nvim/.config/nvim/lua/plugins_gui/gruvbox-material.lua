-- https://github.com/sainnhe/gruvbox-material

-- Gruvbox Material is a modified version of Gruvbox,
-- the contrast is adjusted to be softer in order to protect developers' eyes.

return {
  'sainnhe/gruvbox-material',
  -- lazy = true,
  config = function()
    vim.cmd([[
      let g:gruvbox_material_background = 'hard'
      let g:gruvbox_material_foreground = 'original'
      let g:gruvbox_material_enable_bold = 1
      let g:gruvbox_material_enable_italic = 1
      let g:gruvbox_material_dim_inactive_windows = 1
      let g:gruvbox_material_sign_column_background = 'grey'
      let g:gruvbox_material_ui_contrast = 'low'
      let g:gruvbox_material_statusline_style = 'original'
      let g:gruvbox_material_better_performance = 1
    ]])
  end
}

