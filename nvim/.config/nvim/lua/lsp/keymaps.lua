-- local functions --------------------------------------------------------<<<
-- which-key
local wk = require("which-key")

local function icon_func(ic, col)
  if If_gui() then
    return { icon = ic, color = col }
  else
    return {}
  end
end

-- LSP
local diagnostic_show_flag = true
local function Toggle_diagnostic_show()
  if diagnostic_show_flag then
    vim.diagnostic.hide()
    diagnostic_show_flag = false
  else
    vim.diagnostic.show()
    diagnostic_show_flag = true
  end
end
-- Fin : local functions -------------------------------------------------->>>

-- [L]SP ------------------------------------------------------------------<<<
wk.add({
  {
    '<leader>La',
    vim.lsp.buf.code_action,
    desc = 'LSP: code [a]ction',
    icon = icon_func('', 'blue'),
  },
  {
    '<leader>Ld',
    require('telescope.builtin').lsp_type_definitions,
    desc = 'LSP: Type [d]efinition',
    icon = icon_func('', 'blue'),
  },
  {
    '<leader>Lm',
    vim.diagnostic.open_float,
    desc = 'Open floating diagnostic [m]essage',
    icon = icon_func('󰍥', 'blue'),
  },
  {
    '<leader>Lh',
    Toggle_diagnostic_show,
    desc = 'toggle [h]ide diagnostics',
    icon = icon_func('', 'blue'),
  },
  {
    '<leader>qd',
    vim.diagnostic.setqflist,
    desc = 'open [d]iagnostics [q]uickfix list',
    icon = icon_func("", "blue"),
  },
  {
    '<leader>g[',
     ':Lspsaga diagnostic_jump_prev<return>',
    desc = '[g]o to previous diagnostic message',
    icon = icon_func("", "blue"),
  },
  {
    '<leader>g]',
     ':Lspsaga diagnostic_jump_next<return>',
    desc = '[g]o to next diagnostic message',
    icon = icon_func("", "blue"),
  },
  {
    icon = icon_func("󰌘", "blue"),
    {
      '<leader>gd',
      require('telescope.builtin').lsp_definitions,
      desc = 'LSP: [g]oto [d]efinition',
    },
    {
      '<leader>gD',
      vim.lsp.buf.declaration,
      desc = 'LSP: [g]oto [D]eclaration',
    },
    {
      '<leader>gi',
      require('telescope.builtin').lsp_implementations,
      desc = 'LSP: [g]oto [I]mplementation',
    },
    {
      '<leader>gr',
      require('telescope.builtin').lsp_references,
      desc = 'LSP: [g]oto [r]eferences',
    },
    {
      '<leader>Lr',
      ':Lspsaga rename<return>',
      desc = '[r]ename',
    },
    {
      '<leader>Ls',
      require('telescope.builtin').lsp_document_symbols,
      desc = 'document [s]ymbols',
    },
  },
  {
    '<leader>Lwa',
    vim.lsp.buf.add_workspace_folder,
    desc = '[w]orkspace [a]dd Folder',
    icon = icon_func("󰮝", "blue"),
  },
  {
    '<leader>Lwl',
    function()
      print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end,
    desc = '[w]orkspace [l]ist Folders',
    icon = icon_func("󱧋", "blue"),
  },
  {
    '<leader>Lwr',
    vim.lsp.buf.remove_workspace_folder,
    desc = '[w]orkspace [r]emove Folder',
    icon = icon_func("󰮞", "blue"),
  },
  {
    '<leader>Lws',
    require('telescope.builtin').lsp_dynamic_workspace_symbols,
    desc = '[w]orkspace [s]ymbols',
    icon = icon_func("󰌘", "blue"),
  },
  -- See `:help K` for why this keymap
  {
    '<leader>K',
    ':Lspsaga hover_doc<return>',
    desc = 'Hover Documentation',
    icon = icon_func("󰌘", "blue"),
  },
  {
    '<leader>k',
    vim.lsp.buf.signature_help,
    desc = 'Signature Documentation',
    icon = icon_func("󰌘", "blue"),
  },
})
-- Fin : [L]SP ------------------------------------------------------------>>>

-- The line beneath this is called `modeline`. See `:help modeline`
-- vim: ts=2 sts=2 sw=2 et
