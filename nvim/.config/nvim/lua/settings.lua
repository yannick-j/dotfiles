-- [[ SETTINGS ]]

-- HEADER ------------------------------------------------------------------<<<
--[[

=====================================================================
==================== READ THIS BEFORE CONTINUING ====================
=====================================================================
========                                    .-----.          ========
========         .----------------------.   | === |          ========
========         |.-""""""""""""""""""-.|   |-----|          ========
========         ||                    ||   | === |          ========
========         ||   KICKSTART.NVIM   ||   |-----|          ========
========         ||                    ||   | === |          ========
========         ||                    ||   |-----|          ========
========         ||:Tutor              ||   |:::::|          ========
========         |'-..................-'|   |____o|          ========
========         `"")----------------(""`   ___________      ========
========        /::::::::::|  |::::::::::\  \ no mouse \     ========
========       /:::========|  |==hjkl==:::\  \ required \    ========
========      '""""""""""""'  '""""""""""""'  '""""""""""'   ========
========                                                     ========
=====================================================================
=====================================================================

What is Kickstart?

  Kickstart.nvim is *not* a distribution.
    If you don't know anything about Lua, I recommend taking some time to read through
    a guide. One possible example which will only take 10-15 minutes:
      - https://learnxinyminutes.com/docs/lua/

    After understanding a bit more about Lua, you can use `:help lua-guide` as a
    reference for how Neovim integrates Lua.
    - :help lua-guide
    - (or HTML version): https://neovim.io/doc/user/lua-guide.html

Kickstart Guide:
    MOST IMPORTANTLY, we provide a keymap "<space>sh" to [s]earch the [h]elp documentation,
    which is very useful when you're not exactly sure of what you're looking for.

--]]
-- Fin : HEADER ------------------------------------------------------------>>>

local function If_gui()
  if (vim.env.TERM == 'alacritty') or (vim.env.TERM == 'xterm-256color') or (vim.env.TERM == 'xterm-kitty') or (vim.env.TERM == 'foot') then
    return true
  else
    return false
  end
end

-- python venv
vim.g.python3_host_prog = '/home/yannick/.venv/bin/python'

-- [[ leader keys ]]
-- Set <space> as the leader key
-- See `:help mapleader`
-- NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- [[ nerd font ]]
-- Set to true if you have a Nerd Font installed and selected in the terminal
vim.g.have_nerd_font = true

-- [[ Setting options ]] ---------------------------------------------------<<<
-- See `:help vim.opt`
-- NOTE: You can change these options as you wish!
--  For more options, you can see `:help option-list`

-- Make line numbers default
vim.opt.number = true
-- set relativenumbers by default
vim.opt.relativenumber = true

-- Enable mouse mode, can be useful for resizing splits for example!
if If_gui() then
  vim.opt.mouse = 'a'
end

-- set termguicolors
if If_gui() then
  vim.opt.termguicolors = true
end

-- Don't show the mode, since it's already in the status line
vim.opt.showmode = false

-- Sync clipboard between OS and Neovim.
--  Schedule the setting after `UiEnter` because it can increase startup-time.
--  Remove this option if you want your OS clipboard to remain independent.
--  See `:help 'clipboard'`
if If_gui() then
  vim.schedule(function()
    vim.opt.clipboard = 'unnamedplus'
  end)
end

-- Enable break indent
vim.opt.breakindent = true

-- Save undo history
vim.opt.undofile = true

-- Case-insensitive searching UNLESS \C or one or more capital letters in the search term
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- Keep signcolumn on by default
vim.opt.signcolumn = 'yes'

-- Decrease update time
vim.opt.updatetime = 250

-- Decrease mapped sequence wait time
-- Displays which-key popup sooner
vim.opt.timeoutlen = 300

-- Configure how new splits should be opened
vim.opt.splitright = true
-- vim.opt.splitbelow = true

-- caractères non imprimables
-- Sets how neovim will display certain whitespace characters in the editor.
--  See `:help 'list'`
--  and `:help 'listchars'`
if If_gui() then
  vim.opt.list = true
  vim.opt.listchars = {
    tab = '↦·',
    trail = '·',
    eol = '¶',
    nbsp = '␣'
  }
end

-- Preview substitutions live, as you type!
vim.opt.inccommand = 'split'

-- Show which line your cursor is on
vim.opt.cursorline = true

-- Minimal number of screen lines to keep above and below the cursor.
vim.opt.scrolloff = 10

-- Set completeopt to have a better completion experience
-- :h completeopt
vim.opt.completeopt = 'menuone,preview,noselect,fuzzy'

-- pliage de code
-- :h fold
vim.opt.foldmethod = 'marker'
vim.opt.foldmarker = '---<<<,--->>>'
vim.opt.foldcolumn = '1'

-- colorcolumn
vim.opt.textwidth = 80
vim.opt.colorcolumn = "+1"

-- change de répertoire
vim.opt.autochdir = true

-- orthographe
vim.opt.spelllang = "fr"
vim.opt.spell = true
-- Fin : [[ Setting options ]] --------------------------------------------->>>

-- The line beneath this is called `modeline`. See `:help modeline`
-- vim: ts=2 sts=2 sw=2 et
