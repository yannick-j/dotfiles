-- https://github.com/folke/twilight.nvim

-- Twilight is a Lua plugin for Neovim 0.5 that dims inactive portions of the
-- code you're editing.

return {
  "folke/twilight.nvim",
  opts = {
    -- your configuration comes here
    -- or leave it empty to use the default settings
    -- refer to the configuration section below
  }
}

-- https://github.com/junegunn/limelight.vim
-- Hyperfocus-writing in Vim.
-- return {
--   'junegunn/limelight.vim',
--   cmd = 'Limelight',
-- }
