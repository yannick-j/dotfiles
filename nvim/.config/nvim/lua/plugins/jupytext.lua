-- https://github.com/GCBallesteros/jupytext.nvim

-- Seamlessly open Jupyter Notebooks as there associated
-- plain text alternatives.
-- Powered by jupytext.
-- jupytext.nvim is a lua port of the original jupytext.vim
-- with some additional features and a simpler configuration.

return {
  "GCBallesteros/jupytext.nvim",
  opts = {
    style = "hydrogen",
    output_extension = "auto",  -- Default extension. Don't change unless you know what you are doing
    force_ft = nil,  -- Default filetype. Don't change unless you know what you are doing
    custom_language_formatting = {
      python = {
        extension = "md",
        style = "markdown",
        force_ft = "markdown", -- you can set whatever filetype you want here
      },
    }
  },
  -- Depending on your nvim distro or config you may need to make the loading not lazy
  lazy = false,
}
