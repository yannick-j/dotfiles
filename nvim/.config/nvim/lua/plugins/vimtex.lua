-- https://github.com/lervag/vimtex

-- VimTeX is a modern Vim and Neovim filetype and syntax plugin
-- for LaTeX files.

return {
  'lervag/vimtex',
  dependencies = { 'micangl/cmp-vimtex', },
  lazy = false, -- needed for synctex inverse search
  init = function()
    vim.g.mkdp_filetypes = { "tex" }
    vim.g.vimtex_syntax_conceal_disable = false
  end,
  ft = { "tex" },
  config = function()
    vim.cmd([[
      let g:vimtex_view_method = 'zathura'
      let g:vimtex_syntax_enabled = 1
      " let g:vimtex_view_method = 'skim'
      " let g:vimtex_view_skim_sync = 1
      " let g:vimtex_view_skim_reading_bar = 1
      let g:vimtex_quickfix_open_on_warning = 0 " suppr warnings
      let g:vimtex_doc_handlers = ['vimtex#doc#handlers#texdoc']
      autocmd Filetype tex compiler lacheck
    ]])
  end
}
