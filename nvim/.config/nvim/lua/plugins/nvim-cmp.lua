-- https://github.com/hrsh7th/nvim-cmp

-- A completion engine plugin for neovim written in Lua.
-- Completion sources are installed from external repositories and "sourced".

return {
  'hrsh7th/nvim-cmp',
  dependencies = {
    -- Configs for the Nvim LSP client (:help lsp).
    'neovim/nvim-lspconfig',
    -- nvim-cmp source for filesystem paths.
    'hrsh7th/cmp-path',
    -- nvim-cmp source for buffer words
    'hrsh7th/cmp-buffer',
    -- nvim-cmp source for vim's cmdline.
    'hrsh7th/cmp-cmdline',
    -- Snippet Engine & its associated nvim-cmp source
    -- 'L3MON4D3/LuaSnip',
    -- 'saadparwaiz1/cmp_luasnip',
    'SirVer/ultisnips',
    'quangnguyen30192/cmp-nvim-ultisnips',

    -- Adds LSP completion capabilities
    'hrsh7th/cmp-nvim-lsp',

    -- Adds a number of user-friendly snippets
    -- 'rafamadriz/friendly-snippets',
    'honza/vim-snippets',

    -- lspkind-nvim This tiny plugin adds vscode-like pictograms to neovim built-in lsp
    'onsails/lspkind.nvim',
  },
}
