-- https://github.com/mbbill/undotree

-- The undo history visualizer for VIM

return {
  'mbbill/undotree',
  cmd = 'UndotreeToggle',
}
