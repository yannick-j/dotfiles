-- https://github.com/neovim/nvim-lspconfig

-- Configs for the Nvim LSP client (:help lsp).

return {
  'neovim/nvim-lspconfig',
  dependencies = {
    -- Automatically install LSPs to stdpath for neovim
    'williamboman/mason.nvim',
    'williamboman/mason-lspconfig.nvim',

    -- Useful status updates for LSP
    -- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
    { 'j-hui/fidget.nvim', tag = 'legacy', opts = {} },

    -- Additional lua configuration, makes nvim stuff amazing!
    -- Neovim setup for init.lua and plugin development
    -- with full signature help, docs and completion for the nvim lua API.
    -- 'folke/neodev.nvim',
  },
}
