-- https://github.com/NeogitOrg/neogit

-- A git interface for Neovim, inspired by Magit.

return {
  -- "NeogitOrg/neogit",
  -- dependencies = {
  --   "nvim-lua/plenary.nvim",         -- required
  --   "nvim-telescope/telescope.nvim", -- optional
  --   "sindrets/diffview.nvim",        -- optional
  --   -- "ibhagwan/fzf-lua",              -- optional
  -- },
  -- config = true,
}
