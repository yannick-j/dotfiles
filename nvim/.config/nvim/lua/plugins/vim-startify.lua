-- https://github.com/mhinz/vim-startify

-- vim-startify : The fancy start screen for Vim.

-- functions test gui ------------------------------------------------------<<<
local function If_gui()
  if (vim.env.TERM == 'alacritty') or (vim.env.TERM == 'xterm-256color') or (vim.env.TERM == 'xterm-kitty') or (vim.env.TERM == 'foot') then
    return true
  else
    return false
  end
end
-- Fin : functions test gui ------------------------------------------------>>>

---@diagnostic disable-next-line: undefined-global
if If_gui() then
  vim.cmd([[
    let g:startify_commands = [
        \ {'f': ['File manager', ':Yazi']},
        \ {'o': ['Telescope Oldfiles', 'lua require("telescope.builtin").oldfiles({hidden=true, layout_config={prompt_position="top"}})']},
        \ {'t': ['Telescope find files', 'lua require("telescope.builtin").find_files({hidden=true, layout_config={prompt_position="top"}})']},
        \ {'w': ['VimWiki', 'edit ~/vimwiki/_index.md']},
        \ {'h': ['Tutor','Tutor']},
        \ ]
  ]])
else
  vim.cmd([[
    let g:startify_commands = [
        \ {'o': ['Telescope Oldfiles', 'lua require("telescope.builtin").oldfiles({hidden=true, layout_config={prompt_position="top"}})']},
        \ {'t': ['Telescope find files', 'lua require("telescope.builtin").find_files({hidden=true, layout_config={prompt_position="top"}})']},
        \ {'w': ['VimWiki', 'edit ~/vimwiki/_index.md']},
        \ {'h': ['Tutor','Tutor']},
        \ ]
  ]])
end

return {
  'mhinz/vim-startify',
  config = function()
    vim.cmd([[
      let s:nvim_version = matchstr(execute('version'), 'NVIM v\zs[^\n]*')
      let s:ascii = [
        \ '   ',
        \ '                           _',
        \ '     _ __   ___  _____   _(_)_ __ ___',
        \ '    | `_ \ / _ \/ _ \ \ / / | `_ ` _ \',
        \ '    | | | |  __/ (_) \ V /| | | | | | |',
        \ '    |_| |_|\___|\___/ \_/ |_|_| |_| |_|',
        \ '   ',
        \ '     ' . s:nvim_version
        \]
      let g:startify_custom_header = s:ascii
      " let g:startify_custom_header = startify#pad(split(system('figlet -w 100 neovim 0.10.0'), '\n'))
      let g:startify_custom_footer = 'startify#center(startify#fortune#quote())'
      let g:startify_fortune_use_unicode = 1
      let g:startify_skiplist = [ $HOME .'/vimwiki' ]
      let g:startify_session_persistence = 1
      let g:startify_files_number = 5
      let g:startify_lists = [
          \ { 'type': 'files',     'header': ['   MRU']            },
          \ { 'type': 'sessions',  'header': ['   Sessions']       },
          \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
          \ { 'type': 'commands',  'header': ['   Commands']       },
          \ ]
    ]])
  end
}
