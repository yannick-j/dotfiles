-- https://github.com/iamcco/markdown-preview.nvim

-- Markdown Preview for (Neo)vim
-- Preview Markdown in your modern browser
-- with synchronised scrolling and flexible configuration.
-- install with yarn or npm

return {
  "iamcco/markdown-preview.nvim",
  enabled = vim.fn.executable("npm") == 1,
  cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
  build = "cd app && npm install && git restore .",

  init = function()
    vim.g.mkdp_filetypes = { "markdown" }
    vim.g.mkdp_auto_close = 0
    vim.g.mkdp_command_for_global = 1
    vim.g.mkdp_combine_preview = 1
    vim.g.mkdp_browser = 'chromium'
    vim.g.mkdp_markdown_css = '/home/yannick/.config/nvim/markdown-gruvbox.css'

    local function load_then_exec(cmd)
      return function()
        vim.cmd.delcommand(cmd)
        require("lazy").load({ plugins = { "markdown-preview.nvim" } })
        vim.api.nvim_exec_autocmds("BufEnter", {}) -- commands appear only after BufEnter
        vim.cmd(cmd)
      end
    end

    ---Fixes "No command :MarkdownPreview"
    ---https://github.com/iamcco/markdown-preview.nvim/issues/585#issuecomment-1724859362
    for _, cmd in pairs({ "MarkdownPreview", "MarkdownPreviewStop", "MarkdownPreviewToggle" }) do
      vim.api.nvim_create_user_command(cmd, load_then_exec(cmd), {})
    end
  end,

  config = function()
    vim.cmd([[do FileType]])
    vim.cmd([[
      function OpenMarkdownPreview (url)
        let cmd = "chromium --app=" . shellescape(a:url) . " &"
        silent call system(cmd)
      endfunction
    ]])
    vim.g.mkdp_browserfunc = "OpenMarkdownPreview"
  end,
}
