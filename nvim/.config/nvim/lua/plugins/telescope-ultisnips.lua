-- https://github.com/fhill2/telescope-ultisnips.nvim

-- Integration for ultisnips.nvim with telescope.nvim.

return {
  'fhill2/telescope-ultisnips.nvim',
  lazy = true,
}
