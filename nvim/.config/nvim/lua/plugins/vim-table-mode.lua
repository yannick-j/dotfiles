-- https://github.com/dhruvasagar/vim-table-mode

-- An awesome automatic table creator & formatter
-- allowing one to create neat tables as you type.

return {
  'dhruvasagar/vim-table-mode',
  ft = { "markdown", "pandoc" },
}
