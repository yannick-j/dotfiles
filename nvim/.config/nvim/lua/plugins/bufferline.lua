-- https://github.com/akinsho/bufferline.nvim

-- A snazzy buffer line (with tabpage integration) for Neovim built using lua.

local function If_gui_bufferline()
  if (vim.env.TERM == 'alacritty') or (vim.env.TERM == 'xterm-256color') or (vim.env.TERM == 'xterm-kitty' ) then
    -- NOTE: You should make sure your terminal supports this
    vim.o.termguicolors = true
    local bufferline = require('bufferline')
    bufferline.setup {
      options = {
        style_preset = {
            bufferline.style_preset.no_italic,
            bufferline.style_preset.no_bold
        },
      },
    -- gruvbox
      highlights = {
        fill = {
          bg = '#282828',
        },
        buffer_selected = {
          fg = '#a89984',
          bg = '#504945',
        },
        close_button_selected= {
          fg = '#a89984',
          bg = '#504945',
        },
        modified_selected = {
          bg = '#504945',
        },
      },
    }
  else
    local bufferline = require('bufferline')
    bufferline.setup {
      options = {
        style_preset = bufferline.style_preset.minimal,
        indicator = { style = "none" },
        buffer_close_icon = '',
        modified_icon = '',
        close_icon = '',
        left_trunc_marker = '',
        right_trunc_marker = '',
        show_buffer_icons = false,
        show_buffer_close_icons = false,
        show_close_icon = false,
        show_tab_indicators = false,
        offsets = {
          {
            filetype = "",
            text = "File Explorer",
            text_align = "center",
            separator = false,
          }
        },
        color_icons = false,
      },
    }
  end
end

return {
  -- 'akinsho/bufferline.nvim',
  -- version = "*",
  -- dependencies = 'nvim-tree/nvim-web-devicons',
  -- config = function()
  --   If_gui_bufferline()
  -- end
}
