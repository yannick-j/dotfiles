-- https://github.com/mattn/calendar-vim
-- https://github.com/itchyny/calendar.vim

-- A calendar application for Vim

return {
  -- 'itchyny/calendar.vim',
  'mattn/calendar-vim',
  cmd = 'Calendar',
}
