-- https://github.com/nvim-treesitter/nvim-treesitter

-- Treesitter configurations and abstraction layer for Neovim.
-- Highlight, edit, and navigate code

return {
  'nvim-treesitter/nvim-treesitter',
  dependencies = {
    'nvim-treesitter/nvim-treesitter-textobjects',
  },
  build = ':TSUpdate',
  config = function()
    ---@diagnostic disable-next-line: missing-fields
    require('nvim-treesitter.configs').setup {

      -- A list of parser names, or "all" (the five listed parsers should always be installed)
      -- Add languages to be installed here that you want installed for treesitter
      ensure_installed = {
        "bash",
        "c",
        "html",
        "latex",
        "lua",
        "markdown",
        "markdown_inline",
        "python",
        "query",
        "vim",
        "vimdoc",
        "yaml"
      },

      -- Install parsers synchronously (only applied to `ensure_installed`)
      sync_install = true,

      -- Automatically install missing parsers when entering buffer
      -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
      auto_install = true,

      -- List of parsers to ignore installing (or "all")
      -- ignore_install = { "javascript", "latex" },

      -- Modules and its options go here
      highlight = {
        enable = true,
        -- disable = { "latex" },
        additional_vim_regex_highlighting = true,
      },
      indent = { enable = true },
      incremental_selection = { enable = true },
      textobjects = { enable = true },
    }
  end
}
