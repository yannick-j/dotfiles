-- https://github.com/nvim-telescope/telescope-symbols.nvim

-- telescope-symbols provide its users with the ability
-- of picking symbols and insert them at point.

return {
  'nvim-telescope/telescope-symbols.nvim',
  lazy = true,
}
