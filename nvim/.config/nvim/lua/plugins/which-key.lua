-- https://github.com/folke/which-key.nvim

-- WhichKey is a lua plugin for Neovim 0.5 that displays a popup
-- with possible key bindings of the command you started typing.
-- Heavily inspired by the original emacs-which-key and vim-which-key.

-- breadcrumb = ">", -- symbol used in the command line area that shows your active key combo
-- separator = "", -- symbol used between a key and it's label
-- group = "+", -- symbol prepended to a group

function If_gui()
  if (vim.env.TERM == 'alacritty') or (vim.env.TERM == 'xterm-256color') or (vim.env.TERM == 'xterm-kitty' ) then
    return true
  else
    return false
  end
end

if If_gui() then
  return {
    'folke/which-key.nvim',
    dependencies = {
      "nvim-tree/nvim-web-devicons",
    },
    event = "VeryLazy",
    opts = {
      icons = {
        -- set icon mappings to true if you have a Nerd Font
        mappings = vim.g.have_nerd_font,
        -- If you are using a Nerd Font: set icons.keys to an empty table which will use the
        -- default whick-key.nvim defined Nerd Font icons, otherwise define a string table
        keys = vim.g.have_nerd_font and {} or {
          Up = '<Up> ',
          Down = '<Down> ',
          Left = '<Left> ',
          Right = '<Right> ',
          C = '<C-…> ',
          M = '<M-…> ',
          D = '<D-…> ',
          S = '<S-…> ',
          CR = '<CR> ',
          Esc = '<Esc> ',
          ScrollWheelDown = '<ScrollWheelDown> ',
          ScrollWheelUp = '<ScrollWheelUp> ',
          NL = '<NL> ',
          BS = '<BS> ',
          Space = '<Space> ',
          Tab = '<Tab> ',
          F1 = '<F1>',
          F2 = '<F2>',
          F3 = '<F3>',
          F4 = '<F4>',
          F5 = '<F5>',
          F6 = '<F6>',
          F7 = '<F7>',
          F8 = '<F8>',
          F9 = '<F9>',
          F10 = '<F10>',
          F11 = '<F11>',
          F12 = '<F12>',
        },
      },
    }
  }
else
  return {
    'folke/which-key.nvim',
    dependencies = {
      "nvim-tree/nvim-web-devicons",
    },
    event = "VeryLazy",
    opts = {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
      icons = {
        breadcrumb = ">", -- symbol used in the command line area that shows your active key combo
        separator = "-", -- symbol used between a key and it's label
        group = "+", -- symbol prepended to a group
        ellipsis = ".",
        --- See `lua/which-key/icons.lua` for more details
        --- Set to `false` to disable keymap icons
        ---@type wk.IconRule[]|false
        rules = false,
      },
    },
  }
end
