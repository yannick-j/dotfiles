-- https://github.com/dpelle/vim-Grammalecte

-- Vim-Grammalecte is a plugin that integrates Grammalecte into Vim.
-- Grammalecte is an Open Source grammar checker for French.
-- See http://grammalecte.net for more details about Grammalecte.

return {
  'dpelle/vim-Grammalecte',
  config = function()
    vim.cmd([[
      let g:grammalecte_cli_py='~/.local/bin/Grammalecte/grammalecte-cli.py'
      highlight GrammalecteGrammarError gui=undercurl guisp=Green
      highlight GrammalecteSpellingError gui=undercurl guisp=Orange
    ]])
  end
}
