-- https://github.com/phongvcao/vim-stardict

-- A Vim plugin for looking up meaning of words inside Vim,
-- Bash and Zsh using the StarDict Command-Line Version
-- (SDCV) dictionary program.
-- In addition to opening a Vim split and populating it with the output of
-- StarDict Command-Line Version (SDCV),
-- vim-stardict takes advantage of Vim syntax highlighting and
-- some basic regexes to present the words' definitions to the users
-- in an organized and user-friendly way.


return {
  'phongvcao/vim-stardict',
  cmd = 'StarDictCursor',
}
