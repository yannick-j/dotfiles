-- https://github.com/mfussenegger/nvim-lint
-- https://www.josean.com/posts/neovim-linting-and-formatting

-- An asynchronous linter plugin for Neovim (>= 0.6.0)
-- complementary to the built-in Language Server Protocol support.

return {
    "mfussenegger/nvim-lint",
    event = { "BufReadPre", "BufNewFile" },
    config = function()
        local lint = require("lint")

        lint.linters_by_ft = {
            bash = { "shellcheck" },
            markdown = { "markdownlint-cli2" },
            pandoc = { "markdownlint-cli2" },
            lua = { "luacheck" },
            python = { "flake8"  },
            tex = { "lacheck" },
        }

        vim.keymap.set("n", "<leader>Ll", function()
            lint.try_lint()
        end, { desc = "trigger [l]inting for current file" })

        -- vim.api.nvim_create_autocmd({ "BufWritePost" }, {
        --   callback = function()
        --
        --     -- try_lint without arguments runs the linters defined in `linters_by_ft`
        --     -- for the current filetype
        --     require("lint").try_lint()
        --
        --     -- You can call `try_lint` with a linter name or a list of names to always
        --     -- run specific linters, independent of the `linters_by_ft` configuration
        --     -- require("lint").try_lint("cspell")
        --   end,
        -- })
    end,
}
