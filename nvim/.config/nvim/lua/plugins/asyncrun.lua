-- https://github.com/skywind3000/asyncrun.vim

-- Run Async Shell Commands in Vim 8.0 / NeoVim
-- and Output to the Quickfix Window !!

return {
  'skywind3000/asyncrun.vim',
  cmd = 'AsyncRun',
}
