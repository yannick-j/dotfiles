-- https://github.com/vimwiki/vimwiki

-- Personal Wiki for Vim

return {
  'vimwiki/vimwiki',
  event = 'VeryLazy',
  init = function()
    local l = {}
    l.path = '~/vimwiki'
    l.syntax = 'markdown'
    l.ext = '.md'
    l.index = '_index'
    l.auto_toc = 1
    l.diary_rel_path = 'journal'
    l.diary_index = '_index'
    l.diary_header = 'Journal de bord'
    l.diary_sort = 'desc'
    l.diary_caption_level = 3
    l.auto_diary_index = 1
    vim.g.vimwiki_list = { l }
    vim.g.vimwiki_global_ext = 0
    vim.g.vimwiki_folding = 'expr'
    vim.g.vimwiki_conceallevel = 0
    dict = {
      ['1'] = 'Janvier',
      ['2'] = 'Février',
      ['3'] = 'Mars',
      ['4'] = 'Avril',
      ['5'] = 'Mai',
      ['6'] = 'Juin',
      ['7'] = 'Juillet',
      ['8'] = 'Août',
      ['9'] = 'Septembre',
      ['10'] = 'Octobre',
      ['11'] = 'Novembre',
      ['12'] = 'Décembre',
    }
    vim.g.vimwiki_diary_months = dict
    vim.g.vimwiki_filetypes = { 'markdown' }
  end,
}
