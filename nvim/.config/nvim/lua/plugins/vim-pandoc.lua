-- https://github.com/vim-pandoc/vim-pandoc

-- vim-pandoc provides facilities to integrate Vim with the
-- pandoc document converter and work with documents written in
-- its markdown variant (although textile documents are also supported).

-- vim-pandoc's goals are
-- 1) to provide advanced integration with pandoc,
-- 2) a comfortable document writing environment, and
-- 3) great configurability.

return {
--   'vim-pandoc/vim-pandoc',
--   'vim-pandoc/vim-pandoc-syntax',
}
