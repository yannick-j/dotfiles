-- https://github.com/nvim-lualine/lualine.nvim

-- A blazing fast and easy to configure Neovim statusline written in Lua.

-- local functions ---------------------------------------------------------<<<

local function If_gui()
  if (vim.env.TERM == 'alacritty') or (vim.env.TERM == 'xterm-256color') or (vim.env.TERM == 'xterm-kitty') or (vim.env.TERM == 'foot') then
    return true
  else
    return false
  end
end

-- spell -------------------------------------------------------------------<<<
local function spell()
  if vim.wo.spell == true then -- Note that 'spell' is a window option, so: wo
    return vim.bo.spelllang
  end
  return ''
end
-- Fin : spell ------------------------------------------------------------->>>

-- trunc -------------------------------------------------------------------<<<
--- @param trunc_width number trunctates component when screen width is less then trunc_width
--- @param trunc_len number truncates component to trunc_len number of chars
--- @param hide_width number hides component when window width is smaller then hide_width
--- @param no_ellipsis boolean whether to disable adding '...' at end after truncation
--- return function that can format the component accordingly
local function trunc(trunc_width, trunc_len, hide_width, no_ellipsis)
  return function(str)
    local win_width = vim.fn.winwidth(0)
    if hide_width and win_width < hide_width then return ''
    elseif trunc_width and trunc_len and win_width < trunc_width and #str > trunc_len then
       return str:sub(1, trunc_len) .. (no_ellipsis and '' or '...')
    end
    return str
  end
end
-- Fin : trunc ------------------------------------------------------------->>>

-- asyncrun status ---------------------------------------------------------<<<
local function msg_asyncrun_status()
  local my_msg = '󰓛'
  if vim.g.asyncrun_status == [[running]] then
    my_msg = '󰐊'
  elseif vim.g.asyncrun_status == [[failure]] then
    my_msg = '󰃤'
  elseif vim.g.asyncrun_status == [[succes]] then
    my_msg = '󰸞'
  end
  return my_msg
end
-- Fin : asyncrun status --------------------------------------------------->>>

-- AsyncRun_extension -----------------------------------------------------<<<
local AsyncRun_extension = {
  sections = {
    lualine_a = {
      {
        'mode',
        fmt=trunc(80, 1, 4, true),
        on_click = function() vim.cmd('set spell') end,
      },
      {
        spell,
        on_click = function() vim.cmd('set nospell') end,
      },
    },
    lualine_b = {
      {
        'branch',
        icon = '󰘬',
        on_click = function() vim.cmd('Neogit branch') end,
      },
      {
        'diff',
        on_click = function() vim.cmd('Neogit') end,
      },
    },
    lualine_c = {
      {
        'filename',
        icon = '󰝰',
        fmt=trunc(90, 30, 50, false),
        file_status = true,      -- Displays file status (readonly status, modified status)
        newfile_status = true,  -- Display new file status (new file means no write after created)
        path = 3,                -- 0: Just the filename
                                 -- 1: Relative path
                                 -- 2: Absolute path
                                 -- 3: Absolute path, with tilde as the home directory
                                 -- 4: Filename and parent dir, with tilde as the home directory
        shorting_target = 40,    -- Shortens path to leave 40 spaces in the window
                                 -- for other components. (terrible name, any suggestions?)
        symbols = {
          modified = '+',      -- Text to show when the file is modified.
          readonly = '-',      -- Text to show when the file is non-modifiable or readonly.
          unnamed = 'u', -- Text to show for unnamed buffers.
          newfile = 'n',     -- Text to show for newly created file before first write
        },
        on_click = function() vim.cmd('pwd') end,
      },
    },
    lualine_x = {
      'encoding',
      'fileformat',
      {
        'fancy_filetype',
        ts_icon = "",
        icon_only = true,
        on_click = function() vim.cmd('TSToggle highlight') end,
      },
      {
        msg_asyncrun_status,
        color =
          ---@diagnostic disable-next-line: unused-local
          function(section)
          local status = vim.g.asyncrun_status
            if status == [[running]] then
              return { fg = 106 }
            elseif status == [[failure]] then
              return { bg = 124, fg = 234 }
            elseif status == [[success]] then
              return { fg = 106 }
            else
              return { fg = 223 }
            end
          end,
      },
    },
    lualine_y = {
      {
        require("lazy.status").updates,
        cond = require("lazy.status").has_updates,
        color = { fg = "#d65d0e" },
        on_click = function() vim.cmd('Lazy') end,
      },
      {
        'fancy_lsp_servers',
        on_click = function() vim.cmd('LspInfo') end
      },
      {
        'fancy_diagnostics',
        on_click = function() vim.cmd('Trouble diagnostics toggle filter.buf=0') end
      },
    },
    lualine_z = {
      'location',
      'progress',
    },
  },
  filetypes = { 'python', 'markdown' }
}
-- Fin : AsyncRun_extension ----------------------------------------------->>>

-- fonction Lualine_gui ---------------------------------------------------<<<
local function Lualine_gui()
  require('lualine').setup {
    options = {
      icons_enabled = true,
      theme = 'auto',
      component_separators = '',
      section_separators = '',
      disabled_filetypes = { 'startify' },
      refresh = {
        statusline = 100,
        winbar = 100,
      },
    },
    extensions = {
      AsyncRun_extension,
      -- nvim-dap-ui,
      -- 'trouble',
    },
    sections = {
      lualine_a = {
        {
          'mode',
          fmt=trunc(80, 1, 4, true),
          on_click = function() vim.cmd('set spell') end,
        },
        {
          spell,
          on_click = function() vim.cmd('set nospell') end,
        },
      },
      lualine_b = {
        {
          'branch',
          icon = '󰘬',
          on_click = function() vim.cmd('Neogit branch') end,
        },
        {
          'diff',
          on_click = function() vim.cmd('Neogit') end,
        },
      },
      lualine_c = {
        {
          'filename',
          icon = '󰝰',
          fmt=trunc(90, 30, 50, false),
          file_status = true,     -- Displays file status (readonly status, modified status)
          newfile_status = true,  -- Display new file status (new file means no write after created)
          path = 4,                -- 0: Just the filename
                                   -- 1: Relative path
                                   -- 2: Absolute path
                                   -- 3: Absolute path, with tilde as the home directory
                                   -- 4: Filename and parent dir, with tilde as the home directory
          shorting_target = 40,    -- Shortens path to leave 40 spaces in the window
                                   -- for other components. (terrible name, any suggestions?)
          symbols = {
            modified = '+',      -- Text to show when the file is modified.
            readonly = '-',      -- Text to show when the file is non-modifiable or readonly.
            unnamed = 'u', -- Text to show for unnamed buffers.
            newfile = 'n',     -- Text to show for newly created file before first write
          },
          on_click = function() vim.cmd('pwd') end,
        },
      },
      lualine_x = {
        {
          'fileformat',
          icon_only = true,
        },
        {
          'encoding',
          show_bomb = false,
        },
        {
          'fancy_filetype',
          ts_icon = "",
          icon_only = true,
          on_click = function() vim.cmd('TSToggle highlight') end,
        },
      },
      lualine_y = {
        {
          require("lazy.status").updates,
          cond = require("lazy.status").has_updates,
          color = { fg = "#d65d0e" },
          on_click = function() vim.cmd('Lazy') end,
        },
        {
          'fancy_lsp_servers',
          on_click = function() vim.cmd('LspInfo') end
        },
        {
          'fancy_diagnostics',
          on_click = function() vim.cmd('Trouble diagnostics toggle filter.buf=0') end
        },
      },
      lualine_z = {
        'location',
        'progress',
      },
    },
    winbar = {
      -- lualine_c = {
      --   {
      --     'buffers',
      --     show_filename_only = true, -- Shows shortened relative path when set to false.
      --     hide_filename_extension = true, -- Hide filename extension when set to true.
      --     show_modified_status = true, -- Shows indicator when the buffer is modified.
      --     mode = 0, -- 0: Shows buffer name
      --               -- 1: Shows buffer index
      --               -- 2: Shows buffer name + buffer index
      --               -- 3: Shows buffer number
      --               -- 4: Shows buffer name + buffer number
      --     -- Automatically updates active buffer color to match color of other components (will be overidden if buffers_color is set)
      --     use_mode_colors = true,
      --
      --
      --   },
      -- },
    },
  }
end
-- Fin : fonction Lualine_gui --------------------------------------------->>>

-- fonction Lualine_tty ---------------------------------------------------<<<
local function Lualine_tty()
  require('lualine').setup {
    options = {
      icons_enabled = false,
      theme = 'gruvbox',
      component_separators = '',
      section_separators = '',
      disabled_filetypes = { 'startify' },
      refresh = {
        statusline = 100,
      },
    },
    tabline = {
      lualine_a = {
        {
          'buffers',
          symbols = {
            modified = '[+]',      -- Text to show when the file is modified.
            readonly = '[-]',      -- Text to show when the file is non-modifiable or readonly.
            unnamed = '[No Name]', -- Text to show for unnamed buffers.
            newfile = '[New]',     -- Text to show for newly created file before first write
          }
        }
      },
    },
    sections = {
      lualine_a = {
        {'mode', fmt=trunc(80, 1, 4, true)},
      },
      lualine_b = {
        'branch',
        'diff',
        spell
      },
      lualine_c = {
        {
          'filename',
          fmt=trunc(90, 30, 50, false),
          file_status = true,      -- Displays file status (readonly status, modified status)
          newfile_status = true,  -- Display new file status (new file means no write after created)
          path = 3,                -- 0: Just the filename
                                   -- 1: Relative path
                                   -- 2: Absolute path
                                   -- 3: Absolute path, with tilde as the home directory
                                   -- 4: Filename and parent dir, with tilde as the home directory
          shorting_target = 40,    -- Shortens path to leave 40 spaces in the window
                                   -- for other components. (terrible name, any suggestions?)
          symbols = {
            modified = '+',      -- Text to show when the file is modified.
            readonly = '-',      -- Text to show when the file is non-modifiable or readonly.
            unnamed = 'u', -- Text to show for unnamed buffers.
            newfile = 'n',     -- Text to show for newly created file before first write
          }
        },
      },
      lualine_x = {
        'fileformat',
        'filetype',
      },
      lualine_y = {
      'diagnostics',
      },
      lualine_z = {
        'location',
        'progress',
      },
    },
  }
end
-- Fin : fonction Lualine_tty --------------------------------------------->>>

-- Fin : local functions --------------------------------------------------->>>

return {
  'nvim-lualine/lualine.nvim',
  dependencies = {
    'nvim-tree/nvim-web-devicons',
    "meuter/lualine-so-fancy.nvim",
  },
  config = function()
    if If_gui() then
      Lualine_gui()
    else
      Lualine_tty()
    end
  end
}
