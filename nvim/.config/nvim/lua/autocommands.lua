-- [[ Basic Autocommands ]]
--  See `:help lua-guide-autocommands`

-- Highlight on yank ------------------------------------------------------<<<

-- Highlight when yanking (copying) text
--  Try it with `yap` in normal mode
--  See `:help vim.highlight.on_yank()`
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  desc = 'Highlight when yanking (copying) text',
  group = highlight_group,
  callback = function()
    vim.highlight.on_yank()
  end,
})

-- Fin : Highlight on yank ------------------------------------------------>>>

-- *restore-cursor* *last-position-jump* :h last-position-jump ------------<<<
vim.cmd([[
  augroup RestoreCursor
    autocmd!
    autocmd BufReadPre * autocmd FileType <buffer> ++once
      \ let s:line = line("'\"")
      \ | if s:line >= 1 && s:line <= line("$") && &filetype !~# 'commit'
      \      && index(['xxd', 'gitrebase'], &filetype) == -1
      \ |   execute "normal! g`\""
      \ | endif
  augroup END
]])
-- Fin : *restore-cursor* *last-position-jump* :h last-position-jump ------>>>
