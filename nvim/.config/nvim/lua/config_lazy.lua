
-- local functions ---------------------------------------------------------<<<

-- functions test gui ------------------------------------------------------<<<
local function If_gui()
  if (vim.env.TERM == 'alacritty') or (vim.env.TERM == 'xterm-256color') or (vim.env.TERM == 'xterm-kitty') or (vim.env.TERM == 'foot') then
    return true
  else
    return false
  end
end
-- Fin : functions test gui ------------------------------------------------>>>

local function If_plugins_gui()
  if If_gui() then
    return 'plugins_gui'
  else
    return 'plugins_tty'
  end
end
-- Fin : local functions --------------------------------------------------->>>

-- Bootstrap lazy.nvimBootstrap lazy.nvim ---------------------------------<<<

-- [[ Install `lazy.nvim` plugin manager ]]
--    See `:help lazy.nvim.txt` or
--    https://github.com/folke/lazy.nvim for more info
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  local lazyrepo = "https://github.com/folke/lazy.nvim.git"
  local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
  if vim.v.shell_error ~= 0 then
    vim.api.nvim_echo({
      { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
      { out, "WarningMsg" },
      { "\nPress any key to exit..." },
    }, true, {})
    vim.fn.getchar()
    os.exit(1)
  end
end
vim.opt.rtp:prepend(lazypath)

-- [[ Configure and install plugins ]]
--
--  To check the current status of your plugins, run
--    :Lazy
--
--  You can press `?` in this menu for help. Use `:q` to close the window
--
--  To update plugins you can run
--    :Lazy update
--
-- NOTE: Here is where you install your plugins.
-- Setup lazy.nvim
require('lazy').setup({
  spec = {
    -- import your plugins
    { import = 'plugins' },
    { import = If_plugins_gui() },
  },
  -- Configure any other settings here. See the documentation for more details.
  -- automatically check for plugin updates
  checker = { enabled = true },
  -- disable luarocks and hererocks plugins
  rocks = {
    enabled = false,
    hererocks = false,
  },
})
-- Fin : Bootstrap lazy.nvimBootstrap lazy.nvim --------------------------->>>

-- Automatic backups of lockfile ------------------------------------------<<<

-- Heiker Posted on 30 juil. 2023
-- https://dev.to/vonheikemen/lazynvim-how-to-revert-a-plugin-back-to-a-previous-version-1pdp

-- What I do is use lua to make a backup of the lockfile.
-- I use the autocommand User LazyUpdatePre,
-- this is executed before lazy.nvim updates the plugins.
-- Here I have a function that creates a copy of the lockfile
-- with a time and date as the name.

-- Here is the code.
local lazy_cmds = vim.api.nvim_create_augroup('lazy_cmds', { clear = true })
local snapshot_dir = vim.fn.stdpath('data') .. '/plugin-snapshot'
local lockfile = vim.fn.stdpath('config') .. '/lazy-lock.json'

vim.api.nvim_create_user_command(
  'BrowseSnapshots',
  'edit ' .. snapshot_dir,
  {}
)

vim.api.nvim_create_autocmd('User', {
  group = lazy_cmds,
  pattern = 'LazyUpdatePre',
  desc = 'Backup lazy.nvim lockfile',
  ---@diagnostic disable-next-line: unused-local
  callback = function(event)
    vim.fn.mkdir(snapshot_dir, 'p')
    local snapshot = snapshot_dir .. os.date('/%Y-%m-%dT%H:%M:%S.json')

    vim.loop.fs_copyfile(lockfile, snapshot)
  end,
})

-- If something goes wrong I use the command
-- :BrowseSnapshots
-- to search for the latest lockfile.
-- I usually just grab the commit of the plugin that broke my config,
-- then copy it to the current lockfile.
-- After that, I use the restore command with the name of the plugin.
-- :Lazy restore some-plugin

-- If something went really really bad,
-- I would just copy the whole backup and replace the current lockfile.

-- Fin : Automatic backups of lockfile ------------------------------------>>>
