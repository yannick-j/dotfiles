-- [[ Basic Keymaps ]]
-- Keymaps for better default experience
-- See `:help vim.keymap.set()`

-- local functions --------------------------------------------------------<<<
-- which-key
local wk = require("which-key")

local function icon_func(ic, col)
  if If_gui() then
    return { icon = ic, color = col }
  else
    return {}
  end
end
-- Fin : local functions -------------------------------------------------->>>

-- Clear highlights on search when pressing <Esc> in normal mode
--  See `:help hlsearch`
vim.keymap.set('n', '<Esc>', '<cmd>nohlsearch<CR>')

-- Exit terminal mode in the builtin terminal with a shortcut that is a bit easier
-- for people to discover. Otherwise, you normally need to press <C-\><C-n>, which
-- is not what someone will guess without a bit more experience.
--
-- NOTE: This won't work in all terminal emulators/tmux/etc. Try your own mapping
-- or just use <C-\><C-n> to exit terminal mode
-- vim.keymap.set('t', '<Esc><Esc>', '<C-\\><C-n>', { desc = 'Exit terminal mode' })
-- ne marche pas dans alacritty

-- TIP: Disable arrow keys in normal mode
-- vim.keymap.set('n', '<left>', '<cmd>echo "Use h to move!!"<CR>')
-- vim.keymap.set('n', '<right>', '<cmd>echo "Use l to move!!"<CR>')
-- vim.keymap.set('n', '<up>', '<cmd>echo "Use k to move!!"<CR>')
-- vim.keymap.set('n', '<down>', '<cmd>echo "Use j to move!!"<CR>')

-- Keybinds to make split navigation easier.
--  Use CTRL+<hjkl> to switch between windows
--  See `:help wincmd` for a list of all window commands
-- vim.keymap.set('n', '<C-h>', '<C-w><C-h>', { desc = 'Move focus to the left window' })
-- vim.keymap.set('n', '<C-l>', '<C-w><C-l>', { desc = 'Move focus to the right window' })
-- vim.keymap.set('n', '<C-j>', '<C-w><C-j>', { desc = 'Move focus to the lower window' })
-- vim.keymap.set('n', '<C-k>', '<C-w><C-k>', { desc = 'Move focus to the upper window' })

-- Remap for dealing with word wrap
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- center cursor when move half screen
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

-- ThePrimeAgen greatest remap ever
vim.keymap.set("x", "<leader>P", '"_dP', { desc = "Paste without losing buffer" })

-- which-key groups -------------------------------------------------------<<<

wk.add({
-- bB ---------------------------------------------------------------------<<<
  {
    "<leader>b",
    group = "[b]uffers",
    icon = icon_func("", "red")
  },
-- Fin : bB --------------------------------------------------------------->>>
-- cC ---------------------------------------------------------------------<<<
  {
    "<leader>c",
    group = "[c]alendars",
    icon = icon_func("󰃭", "red")
  },
-- Fin : cC --------------------------------------------------------------->>>
-- dD ---------------------------------------------------------------------<<<
  {
    "<leader>d",
    group = "[d]ap",
    icon = icon_func("󰌘", "red")
  },
-- Fin : dD --------------------------------------------------------------->>>
-- fF ---------------------------------------------------------------------<<<
  {
    "<leader>f",
    ":Yazi<return>",
    desc = '[f]ile manager yazi',
    -- callback = function()
    --   require("ranger-nvim").open(true)
    -- end,
    -- desc = '[f]ile manager ranger',
    icon = icon_func("󰪶", "blue")
  },
-- Fin : fF --------------------------------------------------------------->>>
-- gG ---------------------------------------------------------------------<<<
  {
    "<leader>g",
    group = "[g]oto",
    icon = icon_func("", "red")
  },
  {
    "<leader>G",
    group = "[G]it",
    icon = icon_func("󰊢", "red")
  },
-- Fin : gG --------------------------------------------------------------->>>
-- hH ---------------------------------------------------------------------<<<
  {
    "<leader>h",
    ":Startify<return>",
    desc = "startify",
    icon = icon_func("󰋜", "blue"),
  },
-- Fin : hH --------------------------------------------------------------->>>
-- iI ---------------------------------------------------------------------<<<
  {
    "<leader>i",
    ":e  /home/yannick/.config/nvim/init.lua<cr>",
    desc = "[i]nit.lua",
    icon = icon_func("󰒓", "blue")
  },
-- Fin : iI --------------------------------------------------------------->>>
-- jJ ---------------------------------------------------------------------<<<
  {
    "<leader>j",
    group = "[j]upyter",
    icon = icon_func("", "red")
  },
-- Fin : jJ --------------------------------------------------------------->>>
-- lL ---------------------------------------------------------------------<<<
  {
    "<leader>l",
    mode = { "n", "x" },
    group = "[l]atex",
    icon = icon_func("󰙩", "red")
  },
  {
    -- Nested mappings are allowed and can be added in any order
    -- Most attributes can be inherited or overridden on any level
    -- There's no limit to the depth of nesting
    icon = icon_func("󰌘", "red"),
    {
      "<leader>L",
      mode = { "n", "x" },
      group = "[L]SP",
      icon = icon_func("󰌘", "red"),
    },
    {
      "<leader>LD",
      group = "[L]sp : [D]iagnostics",
    },
    {
      "<leader>Lg",
      group = "[L]sp : [g]oto",
    },
    {
      "<leader>Lw",
      group = "[L]sp : [w]orkspace",
    },
  },
-- Fin : lL --------------------------------------------------------------->>>
-- mM ---------------------------------------------------------------------<<<
  {
    "<leader>m",
    group = "[m]arkdown/pandoc",
    icon = icon_func("", "red"),
  },
-- Fin : mM --------------------------------------------------------------->>>
-- oO ---------------------------------------------------------------------<<<
  {
    "<leader>o",
    group = "[o]rthographe et grammaire",
    icon = icon_func('', 'red'),
  },
-- Fin : oO --------------------------------------------------------------->>>
-- pP ---------------------------------------------------------------------<<<
  {
    mode = { "n", "x" },
    "<leader>p",
    group = "[p]ython",
    icon = icon_func("󰌠", "red")
  },
-- Fin : pP --------------------------------------------------------------->>>
-- qQ ---------------------------------------------------------------------<<<
  {
    "<leader>q",
    group = "[q]uickfix/location list",
    icon = icon_func("", "red"),
  },
  {
    "<leader>ql",
    group = "[l]locationlist",
    icon = icon_func("", "red"),
  },
-- Fin : qQ --------------------------------------------------------------->>>
-- sS ---------------------------------------------------------------------<<<
  {
    icon = icon_func("󰭎", "red"),
    {
      mode = { "n", "x" },
      "<leader>s",
      group = "Telescope [s]earch",
    },
    {
      "<leader>sb",
      group = "Telescope [s]earch [b]uffers",
    },
  },
  {
    "<leader>sG",
    group = "Telescope [s]earch [G]it",
    icon = icon_func("󰊢", "red"),
  },
-- Fin : sS --------------------------------------------------------------->>>
-- tT ---------------------------------------------------------------------<<<
  {
    "<leader>t",
    mode = { "n", "x" },
    group = "vim-[t]able-mode",
    icon = icon_func("", "red"),
  },
  {
    "<leader>T",
    mode = { "x" },
    group = "[T]ableize/{pattern}",
    icon = icon_func("", "blue"),
  },
-- Fin : tT --------------------------------------------------------------->>>
-- uU ---------------------------------------------------------------------<<<
  {
    "<leader>u",
    group = "[u]ltisnips & [u]ndotree",
    icon = icon_func("", "red"),
  },
-- Fin : uU --------------------------------------------------------------->>>
-- wW ---------------------------------------------------------------------<<<
  {
    "<leader>w",
    group = "vim[w]iki",
    icon = icon_func("", "red"),
  },
-- Fin : wW --------------------------------------------------------------->>>
-- xX ----------------------------------------------------------------------<<<
  {
    "<leader>x",
    group = "trouble",
    icon = icon_func("X", "red"),
  },
-- Fin : xX ---------------------------------------------------------------->>>
-- yY ---------------------------------------------------------------------<<<
  {
    "<leader>y",
    group = "[y]scripts",
    icon = icon_func("", "red"),
  },
-- Fin : yY --------------------------------------------------------------->>>
})
-- Fin : which-key -------------------------------------------------------->>>

-- [b]uffers --------------------------------------------------------------<<<
wk.add({
  {
    "<leader>bn",
    ":bnext<return>",
    desc = '[n]next  buffer',
    icon = icon_func("", "blue"),
  },
  {
    "<leader>bp",
    ":bprevious<return>",
    desc = '[p]revious  buffer',
    icon = icon_func("", "blue"),
  },
  {
    "<leader>bd",
    ":bdelete<return>",
    desc = '[d]elete  buffer',
    icon = icon_func("", "blue"),
  },
})
-- Fin : [b]uffers -------------------------------------------------------->>>

-- [C]alendars ------------------------------------------------------------<<<
wk.add({
  {
    icon = icon_func('󰃭', 'blue'),
    {
      "<leader>cc",
      ":Calendar -split=vertical<return>",
      desc = "[c]alendar-vim",
    },
    {
      "<leader>ck",
      ":set splitright | vsplit | startinsert | terminal ikhal<return>",
      desc = "i[k]hal",
    },
  }
})
-- Fin : [C]alendars ------------------------------------------------------>>>

-- [d]ap -------------------------------------------------------------------<<<
local dap = require "dap"
local py = require "dap-python"
local ui = require "dapui"
local wi = require "dap.ui.widgets"
wk.add({
  {
    icon = icon_func('󰌘', 'blue'),
    {
      "<leader>db",
      dap.toggle_breakpoint,
      desc = "[d]ap toggle [b]reakpoint",
    },
    {
      "<leader>dc",
      dap.continue,
      desc = "[d]ap [c]ontinue",
    },
    {
      "<leader>dh",
      wi.hover,
      desc = "[d]ap [h]over",
    },
    {
      mode = "v",
      "<leader>ds",
      py.continue,
      desc = "[d]ap debug [s]election",
    },
    {
      "<leader>dt",
      ui.toggle,
      desc = "[d]ap [t]oggle ui",
    },
  }
})
-- Fin : [d]ap ------------------------------------------------------------->>>

-- [G]itsigns --------------------------------------------------------------<<<
wk.add({
  {
    "<leader>Gs",
    "<cmd>Gitsigns toggle_signs<return>",
    desc = 'toggle git [s]igns',
    icon = icon_func("󰊢", "blue"),
  },
  {
    "<leader>Gn",
    "<cmd>Gitsigns toggle_numhl<return>",
    desc = 'toggle git [n]umhl',
    icon = icon_func("󰊢", "blue"),
  },
  {
    "<leader>Gl",
    "<cmd>Gitsigns toggle_linehl<return>",
    desc = 'toggle git [l]inehl',
    icon = icon_func("󰊢", "blue"),
  },
  {
    "<leader>Gw",
    "<cmd>Gitsigns toggle_word_diff<return>",
    desc = 'toggle git [w]ord diff',
    icon = icon_func("󰊢", "blue"),
  },
})
-- Fin : [G]itsigns -------------------------------------------------------->>>

-- LaTeX ------------------------------------------------------------------<<<
vim.api.nvim_create_autocmd("FileType", {
  pattern = "tex",
  callback = function()
    wk.add({
-- aA ---------------------------------------------------------------------<<<
      {
        "<leader>la",
        "<plug>(vimtex-context-menu)",
        desc = "vimtex-context-menu",
        icon = icon_func('󰮫', 'blue'),
      },
-- Fin : aA --------------------------------------------------------------->>>
-- cC ---------------------------------------------------------------------<<<
      {
        "<leader>lc",
        group = "vimtex-[c]lean",
        desc = "vimtex-[c]lean",
        icon = icon_func('󰃢', 'red'),
      },
      {
        icon = icon_func('󰃢', 'blue'),
        {
          "<leader>lcc",
          "<plug>(vimtex-clean)",
          desc = "vimtex-[c]lean",
        },
        {
          "<leader>lcf",
          "<plug>(vimtex-clean-full)",
          desc = "vimtex-[c]lean-[f]ull",
        },
      },
-- Fin : cC --------------------------------------------------------------->>>
-- eE ---------------------------------------------------------------------<<<
      {
        "<leader>le",
        "<plug>(vimtex-errors)",
        desc = "vimtex-[e]rrors",
        icon = icon_func('', 'blue'),
      },
-- Fin : eE --------------------------------------------------------------->>>
-- gG ---------------------------------------------------------------------<<<
      {
        "<leader>lg",
        group = "vimtex-status",
        desc = "vimtex-status",
        icon = icon_func('󱖫', 'red'),
      },
      {
        icon = icon_func('󱖫', 'blue'),
        {
          "<leader>lgg",
          "<plug>(vimtex-status)",
          desc = "vimtex-status",
        },
        {
          "<leader>lga",
          "<plug>(vimtex-status-all)",
          desc = "vimtex-status-[a]ll",
        },
      },
-- Fin : gG --------------------------------------------------------------->>>
-- iI ---------------------------------------------------------------------<<<
      {
        "<leader>li",
        group = "vimtex-info",
        desc = "vimtex-[i]nfo",
        icon = icon_func('󰋼', 'red'),
      },
      {
        icon = icon_func('󰋼', 'blue'),
        {
          "<leader>lii",
          "<plug>(vimtex-info)",
          desc = "vimtex-[i]nfo",
        },
        {
          "<leader>lif",
          "<plug>(vimtex-info-full)",
          desc = "vimtex-[i]nfo-[f]ull",
        },
      },
-- Fin : iI --------------------------------------------------------------->>>
-- kK ---------------------------------------------------------------------<<<
      {
        "<leader>lk",
        group = "vimtex-stop",
        desc = "vimtex-stop",
        icon = icon_func('󰙦', 'red'),
      },
      {
        icon = icon_func('󰙦', 'blue'),
        {
          "<leader>lkk",
          "<plug>(vimtex-stop)",
          desc = "vimtex-stop",
        },
        {
          "<leader>lka",
          "<plug>(vimtex-stop-all)",
          desc = "vimtex-stop-[a]ll",
        },
      },
-- Fin : kK --------------------------------------------------------------->>>
-- lL ---------------------------------------------------------------------<<<
      {
        icon = { icon = "", color = "blue" },
        {
          "<leader>ll",
          "<plug>(vimtex-compile)",
          desc = "vimtex-compile",
        },
        {
          "<leader>ll",
          "<plug>(vimtex-compile-selected)",
          mode = { "x" },
          desc = "vimtex-compile-selected",
        },
      },
-- Fin : lL --------------------------------------------------------------->>>
-- mM ---------------------------------------------------------------------<<<
      {
        "<leader>lm",
        "<plug>(vimtex-imaps-list)",
        desc = "vimtex-i[m]aps-list",
        icon = icon_func('󰀫', 'blue'),
      },
-- Fin : mM --------------------------------------------------------------->>>
-- oO ---------------------------------------------------------------------<<<
      {
        "<leader>lo",
        "<plug>(vimtex-compile-output)",
        desc = "vimtex-compile-[o]utput",
        icon = icon_func('', 'blue'),
      },
-- Fin : oO --------------------------------------------------------------->>>
-- qQ ---------------------------------------------------------------------<<<
      {
        "<leader>lq",
        "<plug>(vimtex-log)",
        desc = "vimtex-log",
        icon = icon_func('', 'blue'),
      },
-- Fin : qQ --------------------------------------------------------------->>>
-- rR ---------------------------------------------------------------------<<<
      {
        "<leader>lr",
        "<plug>(vimtex-reverse-search)",
        desc = "vimtex-[r]everse-search",
        icon = icon_func('', 'blue'),
      },
-- Fin : rR --------------------------------------------------------------->>>
-- sS ---------------------------------------------------------------------<<<
      {
        "<leader>ls",
        "<plug>(vimtex-toggle-main)",
        desc = "vimtex-toggle-main",
      },
-- Fin : sS --------------------------------------------------------------->>>
-- tT ---------------------------------------------------------------------<<<
      {
        "<leader>lt",
        "<plug>(vimtex-toc-toggle)",
        desc = "vimtex-[t]oc-toggle",
        icon = icon_func('󰠶', 'blue'),
      },
-- Fin : tT --------------------------------------------------------------->>>
-- vV ---------------------------------------------------------------------<<<
      {
        "<leader>lv",
        "<plug>(vimtex-view)",
        desc = "vimtex-[v]iew",
        icon = icon_func('󰈈', 'blue'),
      },
-- Fin : vV --------------------------------------------------------------->>>
-- xX ---------------------------------------------------------------------<<<
      {
        icon = icon_func('󰑓', 'blue'),
        {
          "<leader>lx",
          "<plug>(vimtex-reload)",
          desc = "vimtex-reload",
        },
        {
          "<leader>lX",
          "<plug>(vimtex-reload-state)",
          desc = "vimtex-reload-state",
        },
      },
-- Fin : xX --------------------------------------------------------------->>>
    })
  end
})
-- Fin : LaTeX ------------------------------------------------------------>>>

-- quickfix list et location list -----------------------------------------<<<
wk.add({
-- quickfix list ----------------------------------------------------------<<<
  {
    "<leader>qo",
    ":copen<return>",
    desc = "[o]pen quickfix list",
    icon = icon_func('', 'blue'),
  },
  {
    "<leader>qc",
    ":cclose<return>",
    desc = "[c]lose quickfix list",
    icon = icon_func('', 'blue'),
  },
  {
    "<leader>qn",
    ":cnext<return>",
    desc = "[n]ext error",
    icon = icon_func('', 'blue'),
  },
  {
    "<leader>qp",
    ":cprevious<return>",
    desc = "[p]revious error",
    icon = icon_func('', 'blue'),
  },
-- Fin : quickfix list ---------------------------------------------------->>>
-- location list ----------------------------------------------------------<<<
  {
    "<leader>qlo",
    ":lopen<return>",
    desc = "[o]pen location list",
    icon = icon_func('', 'blue'),
  },
  {
    "<leader>qlc",
    ":lclose<return>",
    desc = "[c]lose location list",
    icon = icon_func('', 'blue'),
  },
  {
    "<leader>qln",
    ":lnext<return>",
    desc = "[n]ext error",
    icon = icon_func('', 'blue'),
  },
  {
    "<leader>qlp",
    ":lprevious<return>",
    desc = "[p]revious error",
    icon = icon_func('', 'blue'),
  },
-- Fin : location list ---------------------------------------------------->>>
})
-- Fin : quickfix list et location list ----------------------------------->>>

-- orthographe & grammaire ------------------------------------------------<<<
local function Toggle_spell()
  if vim.wo.spell == true then
    vim.wo.spell = false
  else
    vim.wo.spell = true
  end
end
wk.add({
  {
    "<leader>os",
    Toggle_spell,
    desc = "toggle [s]pell",
  },
  {
    "<leader>oo",
    ":GrammalecteCheck<CR>",
    desc = "Grammalecte check",
    icon = icon_func('', 'blue'),
  },
  {
    "<leader>oc",
    ":GrammalecteClear<CR>",
    desc = "Grammalecte [c]lear",
    icon = icon_func('', 'blue'),
  },
  {
    "<leader>od",
    ":StarDictCursor<CR>",
    desc = "[d]ictionnaire sdcv",
    icon = icon_func('󰘝', 'blue'),
  },
})
-- Fin : orthographe & grammaire ------------------------------------------>>>

-- python -----------------------------------------------------------------<<<
vim.api.nvim_create_autocmd("FileType", {
  pattern = "python",
  callback = function()
    vim.keymap.set(
      "n",
      "<leader>pp",
      ":w<CR>:AsyncRun python3 %<CR>",
      { desc = "run [p]ython code" }
    )
    vim.keymap.set(
      "n",
      "<leader>ps",
      ":AsyncStop<bar>sleep 100m<bar>let g:asyncrun_status=''<bar>cclose<CR>",
      { desc = "[s]top python code" }
    )
    vim.keymap.set(
      "n",
      "<leader>pl",
      ":w<CR>:AsyncRun pylint %<CR>:copen<CR>",
      { desc = "lint [p]ython code with pylint" }
    )
    vim.keymap.set(
      "n",
      "<leader>pf",
      ":w<CR>:AsyncRun flake8 %<CR>:copen<CR>",
      { desc = "lint [p]ython code with flake8" }
    )
    vim.keymap.set(
      "n",
      "<leader>pv",
      ":AsyncRun source ~/python/.venv/bin/activate<CR>",
      { desc = "select python [v]irtual environment" }
    )
  end
})
wk.add({
  {
    mode = 'x',
    "<leader>ps",
    ":AsyncRun python3<CR>",
    { desc = "run [s]elected python code" }
  },
})
-- Fin : python ----------------------------------------------------------->>>

-- Telescope --------------------------------------------------------------<<<
-- See `:help telescope.builtin`
vim.keymap.set('n', '<leader>so', require('telescope.builtin').oldfiles,
  { desc = 'search recently [o]pened files' })
vim.keymap.set('n', '<leader>sbl', require('telescope.builtin').buffers,
  { desc = 'search in [b]uffers [l]ist' })

vim.keymap.set('n', '<leader>sbf', function()
  -- You can pass additional configuration to telescope to change theme, layout, etc.
  require('telescope.builtin').current_buffer_fuzzy_find(
    require('telescope.themes').get_dropdown {
      winblend = 10,
      previewer = true,
    }
  )
end, { desc = 'Fuzzily [s]earch in current buffer' })

vim.keymap.set('n', '<leader>sGf', require('telescope.builtin').git_files,
  { desc = '[s]earch [G]it [f]iles' })

vim.keymap.set('n', '<leader>sf', function()
  require('telescope.builtin').find_files(
    require('telescope.themes').get_dropdown {
      previewer = true,
    }
  )
end, { desc = 'search [f]iles' })

vim.keymap.set('n', '<leader>s/', function()
  require('telescope.builtin').find_files({
    cwd = '/',
    require('telescope.themes').get_dropdown {
      previewer = true,
    }
  })
end, { desc = 'search root files' })

vim.keymap.set('n', '<leader>sy', function()
  require('telescope.builtin').find_files({
    cwd = '~',
    require('telescope.themes').get_dropdown {
      previewer = true,
    }
  })
end, { desc = 'search home files' })

vim.keymap.set('n', '<leader>sh', require('telescope.builtin').help_tags,
  { desc = 'search [h]elp' })
vim.keymap.set('n', '<leader>sw', require('telescope.builtin').grep_string,
  { desc = 'search current [w]ord' })
vim.keymap.set('n', '<leader>sg', require('telescope.builtin').live_grep,
  { desc = 'search by [g]rep' })
vim.keymap.set('n', '<leader>sGg', ':LiveGrepGitRoot<cr>',
  { desc = 'search by [g]rep on [G]it Root' })
vim.keymap.set('n', '<leader>sd', require('telescope.builtin').diagnostics,
  { desc = 'search [d]iagnostics' })
vim.keymap.set('n', '<leader>sr', require('telescope.builtin').resume,
  { desc = 'search [r]esume' })
vim.keymap.set('n', '<leader>ss', require('telescope.builtin').symbols,
  { desc = 'search [s]ymbols' })
vim.keymap.set('n', '<leader>su', ':Telescope ultisnips<CR>',
  { desc = 'search [u]ltisnips' })
-- Fin : Telescope -------------------------------------------------------->>>

-- markdown ---------------------------------------------------------------<<<
vim.api.nvim_create_autocmd("FileType", {
  pattern = {"markdown", "pandoc", "vimwiki" },
  callback = function()
  wk.add({
    {
      "<leader>mm",
      ":MarkdownPreviewToggle<CR>",
      desc = "[m]arkdown preview toggle",
    },
    {
      "<leader>mv",
      ":silent !zathura %:r.pdf &<CR>",
      desc = "[v]iew pdf",
      icon = icon_func('', 'blue'),
    },
    {
      "<leader>mV",
      ":silent !librewolf %:r.html &<CR>",
      desc = "[v]iew html",
      icon = icon_func('', 'blue'),
    },
    {
      "<leader>mp",
      ":w<CR>:AsyncRun pandoc --pdf-engine=pdflatex -V papersize:a4 -o %:r.pdf %<CR>",
      desc = "make [p]df",
      icon = icon_func('', 'blue'),
    },
    {
      "<leader>mb",
      ":w<CR>:AsyncRun pandoc --pdf-engine=pdflatex -t beamer -o %:r.pdf % -i<CR>",
      desc = "make slide [p]df",
      icon = icon_func('󰐩', 'blue'),
    },
    {
      "<leader>mr",
      ":w<CR>:AsyncRun pandoc -s --lua-filter='/home/yannick/.local/bin/include-code-files/include-code-files.lua' -t revealjs -o %:r.html % -V theme=white --katex='https://cdn.jsdelivr.net/npm/katex@0.16.9/dist/'<CR>",
      desc = "html [r]evealjs",
      icon = icon_func('', 'blue'),
    },
    {
      "<leader>md",
      ":w<CR>:AsyncRun pandoc -o %:r.docx %<CR>",
      desc = "make [d]ocx",
      icon = icon_func('󰍲', 'blue'),
    },
    {
      "<leader>mh",
      "mh :w<CR>:AsyncRun pandoc -s -o %:r.html %<CR>",
      desc = "make [h]tml",
      icon = icon_func('', 'blue'),
    },
    {
      "<leader>mP",
      ":w<CR>:AsyncRun xterm -e patat -w % &<CR>",
      desc = "make [Pa]tat",
      icon = icon_func('', 'blue'),
    },
    {
      "<leader>ms",
      ":AsyncStop<bar>sleep 100m<bar>let g:asyncrun_status=''<bar>cclose<CR>",
      desc = "[s]top markdown preview",
      icon = icon_func('', 'blue'),
    },
  })
  end
})
-- Fin : markdown --------------------------------------------------------->>>

-- jupyter ----------------------------------------------------------------<<<
vim.api.nvim_create_autocmd("FileType", {
  pattern = { "markdown" },
  callback = function()
  wk.add({
    {
      "<leader>jj",
      ":AsyncRun source ~/python/.venv/bin/activate && jupyter notebook %:r.ipynb<return>",
      desc = "start [j]upyter notebook",
      icon = icon_func('󰖟', 'blue'),
    },
    {
      "<leader>js",
      ":AsyncStop<bar>sleep 100m<bar>let g:asyncrun_status=''<bar>cclose<CR>",
      desc = "[s]top [j]upyter notebook",
      icon = icon_func('', 'blue'),
    },
  })
  end
})
-- Fin : jupyter ---------------------------------------------------------->>>

-- ultisnips & undotree ---------------------------------------------------<<<
vim.keymap.set('n', '<leader>ue', ':UltiSnipsEdit<CR>',
{ desc = '[u]ltisnips[e]dit' })
vim.keymap.set('n', '<leader>ur', ':call UltiSnips#RefreshSnippets()<CR>',
{ desc = '[u]ltisnips[r]efresh' })
vim.keymap.set('n', '<leader>ut', vim.cmd.UndotreeToggle,
{ desc = '[u]ndo[t]tree' })

-- Fin : ultisnips & undotree --------------------------------------------->>>

-- scripts y --------------------------------------------------------------<<<
wk.add({
  {
    "<leader>yc",
    ":set splitright | vsplit | startinsert | terminal ycnet %<return>",
    desc = "[yc]net",
    icon = icon_func('󰖟', 'blue'),
  },
})
vim.api.nvim_create_autocmd("FileType", {
  pattern = "tex",
  callback = function()
    wk.add({
      {
        "<leader>yf",
        ":!ylifig <cfile><CR>",
        desc = "insert fig (ylifig)",
        icon = icon_func('', 'blue'),
      },
      {
        "<leader>yt",
        ":!ylitab <cfile><CR>",
        desc = "insert tab (ylitab)",
        icon = icon_func('', 'blue'),
      },
      {
        "<leader>yl",
        ":set splitright | vsplit | startinsert | terminal ylnet %<return>",
        desc = "[yl]net",
        icon = icon_func('󰖟', 'blue'),
      },
    })
  end
})
-- Fin : scripts y -------------------------------------------------------->>>

-- The line beneath this is called `modeline`. See `:help modeline`
-- vim: ts=2 sts=2 sw=2 et
