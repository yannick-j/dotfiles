#!/usr/bin/env sh

find . -type d -name '@eaDir' -print0 | \
	xargs --no-run-if-empty --verbose --null rm --recursive --verbose
