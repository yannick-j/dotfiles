#!/bin/bash
#### Description: synchronisation
#### Dépendances : wofi, rsync,  mailsync
#### Yannick version 4.0
#### mercredi 29 juin

# fonctions ---------------------------------------------------------------<<<

# couleurs ----------------------------------------------------------------<<<

_fn_couleur() {
    case $1 in
        normal) echo -ne "\033[0m";;
        rouge) echo -ne "\033[31m";;
        vert) echo -ne "\033[32m";;
        jaune) echo -ne "\033[33m";;
        bleu) echo -ne "\033[34m";;
        magenta) echo -ne "\033[35m";;
        cyan) echo -ne "\033[36m";;
        gris) echo -ne "\033[37m";;
        hi_rouge) echo -ne "\033[91m";;
        hi_vert) echo -ne "\033[92m";;
        hi_jaune) echo -ne "\033[93m";;
        hi_bleu) echo -ne "\033[94m";;
        hi_magenta) echo -ne "\033[95m";;
        hi_cyan) echo -ne "\033[96m";;
        blanc) echo -ne "\033[97m";;
    esac
}

# fin couleurs ------------------------------------------------------------>>>

# ligne -------------------------------------------------------------------<<<
_fn_ligne() {
    local str
    str=$(printf "%0.s$2" $(seq 1 "$1")) # Fill $str with $1 characters $2
    echo "$str" # Output content of $str to terminal
}
# fin ligne --------------------------------------------------------------->>>

# titres ------------------------------------------------------------------<<<

_fn_titre() {
    # arg $1 : niveau de titre
    # arg $2 : titre
    _fn_couleur hi_bleu
    local n
    n=${#2} # number of characters of $2
    case $1 in
        0)
            _fn_ligne $((n+4)) '#'
            echo -n '# '
            echo -n "$2"
            echo ' #'
            _fn_ligne $((n+4)) '#';;
        1)
            _fn_ligne "$n" '='
            echo "$2"
            _fn_ligne "$n" '=';;
        2)
            _fn_ligne "$n" '-'
            echo "$2"
            _fn_ligne "$n" '-';;
        3)
            echo "$2"
            _fn_ligne "$n" '-';;
    esac
    _fn_couleur normal
}

# fin titres -------------------------------------------------------------->>>

# message -----------------------------------------------------------------<<<
_fn_message() {
    # arg $1 : message
    # arg $2 : couleur
    # arg $3 : titre
    # arg $4 : niveau de titre
    local n
    n="$4"
    n=${n:-3}
    if [ -n "$3" ]; then _fn_titre "$n" "$3"; fi
    if [ -n "$2" ]; then _fn_couleur "$2"; fi
    echo "$1"
    _fn_couleur normal
}
# fin message ------------------------------------------------------------->>>

# message_ok -------------------------------------------------------------<<<
_fn_message_ok() {
    _fn_message 'OK' hi_vert
}
# fin message_ok --------------------------------------------------------->>>

# oui_ou_non ? ------------------------------------------------------------<<<

_fn_oui_ou_non() {
    # arg $1 : message
    # arg $2 : choix par défaut
    local oui_non
    _fn_couleur vert
    case $2 in
        0) read -rp "$1 ? [oui/NON]: $(_fn_couleur magenta)" oui_non;;
        1) read -rp "$1 ? [OUI/non]: $(_fn_couleur magenta)" oui_non;;
    esac
    oui_non=${oui_non:-$2}
    _fn_couleur normal
    case $oui_non in
        [Nn0]*) return 1  ;;
        [OoYy1]*) return 0 ;;
    esac
}

# fin oui_ou_non ? -------------------------------------------------------->>>

# message d'erreur --------------------------------------------------------<<<
_fn_erreur() { _fn_message "$1" 'hi_rouge'; }
# fin message d'erreur ---------------------------------------------------->>>

# fonction test_ok --------------------------------------------------------<<<
_fn_test_ok() {
    if [ "$?" = 1 ]; then
        _fn_erreur 'Pas fait'
    else
        _fn_message_ok
    fi
}
# fin fonction test_ok ---------------------------------------------------->>>

# fonction action ---------------------------------------------------------<<<
_fn_action() {
    # arg $1 : commande
    # arg $2 : titre
    # arg $3 : niveau de titre
    local n
    n=$3
    n=${n:-2}
    if [[ -n "$2" ]]; then _fn_titre "$n" "$2"; fi
    _fn_message "$1" jaune
    _fn_oui_ou_non 'Confirmer' 0 && eval "$1"
    _fn_test_ok
}
# fin fonction action ----------------------------------------------------->>>

# fin fonctions ----------------------------------------------------------->>>

# options
mailsync='󰺻  󰅟 : mailsync'
vdirsyncer='  󰅟 : vdirsyncer'
lyceeUSB='󰑴  󱊞 : rsync lycee  USB'
USBlycee='󱊞  󰑴 : rsync USB  lycee'
NASmusic='󰅟   : rsync NAS  ~/audio/musique/'
musicNAS='  󰅟 : rsync ~/audio/musique/  NAS'
NASpodcasts='󰅟   : rsync NAS  ~/audio/podcasts/'
podcastsNAS='  󰅟 : rsync ~/audio/podcasts/  NAS'

liste="$mailsync\n$vdirsyncer\n$lyceeUSB\n$USBlycee\n$NASmusic\n$musicNAS\n$NASpodcasts\n$podcastsNAS"

choix=$(echo -e "$liste" | \
    wofi --insensitive --lines 10 --width 640 --dmenu \
    --prompt '󰓦 wofi-sync' 2>/dev/null)

case $choix in

    "$mailsync") mailsync;;

    "$vdirsyncer") _fn_titre 1 "$vdirsyncer sync"
        _fn_message 'vdirsyncer sync' jaune
        vdirsyncer sync
        _fn_titre 2 'FIN'
        read -r;;

    "$lyceeUSB") _fn_titre 1 "$lyceeUSB"
        _fn_action \
            'rsync -avh --progress --delete /home/yannick/documents/lycee/ /media/yannick/YANNICK/Lycée/' \
            'rsync documents/lycee/ vers /media/yannick/YANNICK/Lycée' 2
        _fn_titre 2 'FIN'
        read -r;;

    "$USBlycee") _fn_titre 1 "$USBlycee"
        _fn_action \
            'rsync -avh --progress /media/yannick/YANNICK/Lycée/ /home/yannick/documents/lycee/' \
            'rsync /media/yannick/YANNICK/Lycée vers documents/lycee/' 2
        _fn_titre 2 'FIN'
        read -r;;

    "$NASmusic" ) ~/audio/rsync-music.sh -d
        _fn_titre 2 'FIN'
        read -r;;

    "$musicNAS" ) ~/audio/rsync-music.sh -u
        _fn_titre 2 'FIN'
        read -r;;

    "$NASpodcasts" ) ~/audio/rsync-podcasts.sh -d
        _fn_titre 2 'FIN'
        read -r;;

    "$podcastsNAS" ) ~/audio/rsync-podcasts.sh -u
        _fn_titre 2 'FIN'
        read -r;;

    *) exit 0;;

esac

