#!/bin/bash

# gruvbox-dark theme
# url github.com/raindeer44/gruvbox-tty
# raindeer44 <github.com/raindeer44>
# based on gruvbox.vim by morhetz <github.com/morhetz>

if [ "$TERM" = "linux" ]; then
    # echo -en "\e]P0282828" #bg
    echo -en "\e]P1cc241d" #red
    echo -en "\e]P298971a" #green
    echo -en "\e]P3d79921" #yellow
    echo -en "\e]P4458588" #blue
    echo -en "\e]P5b16286" #magenta
    echo -en "\e]P6689d6a" #cyan
    echo -en "\e]P7a89984" #gray
    echo -en "\e]P8928374" #lightgray
    echo -en "\e]P9fb4934" #lightred
    echo -en "\e]PAb8bb26" #lightgreen
    echo -en "\e]PBfabd2f" #lightyellow
    echo -en "\e]PC83a598" #lightblue
    echo -en "\e]PDd3869b" #lightmagenta
    echo -en "\e]PE8ec07c" #lightcyan
    echo -en "\e]PFebdbb2" #fg
    echo -en "\e]P01d2021" #bg0_h
    # echo -en "\e]P83c3836" #bg1
    # echo -en "\e]P8504945" #bg2
    # echo -en "\e]P8665c54" #bg3
    # echo -en "\e]P87c6f64" #bg4
    # echo -en "\e]P832302f" #bg0_s
    # echo -en "\e]P7a89984" #fg4
    # echo -en "\e]P7bdae93" #fg3
    # echo -en "\e]P7d5c4a1" #fg2
    # echo -en "\e]P7fbf1c7" #fg0
    clear #for background artifacting
fi

# Gruvbox dark hard
# echo "29,204,152,215,69,177,104,168,146,251,184,250,131,211,142,235
# 32,36,151,153,133,98,157,153,131,73,187,189,165,134,192,219
# 33,29,26,33,136,134,106,132,116,52,38,47,152,155,124,178" | setvtrgb -
# clear #for background artifacting

# Default vga
# 0,170,0,170,0,170,0,170,85,255,85,255,85,255,85,255
# 0,0,170,85,0,0,170,170,85,85,255,255,85,85,255,255
# 0,0,0,0,170,170,170,170,85,85,85,85,255,255,255,255
# exit 0
