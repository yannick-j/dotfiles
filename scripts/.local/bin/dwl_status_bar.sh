#!/bin/bash

# thème

color_good="#98971a"
color_degraded="#d65d0e"
color_bad="#cc241d"
# color_bg2="#504945"
color_gray="#928374"
color_alert="#fb4934"

separator="|"

# clavier

# keyboard_layout=$(setxkbmap -query | awk '/layout/ {print $2}')
capslock_state=$(cat /sys/class/leds/input0::capslock/brightness)
if [[ $capslock_state == 1 ]]; then
    color=$color_alert
    keyboard_status="^fg($color)󰘲 ^fg()$separator "
fi

# connection ethernet

eth_status=""
eth_ip=$(nmcli device show eth0 | awk '/IP4.ADDRESS/ {print $2}' | cut -d/ -f 1)

if [[ -n "$eth_ip" ]]; then
    color=$color_good
    eth_speed=$(ethtool eth0 2>/dev/null | awk '/Speed/ {print $2}')

    case $eth_speed in
        1000Mb/s)
            color=$color_good;;
        100Mb/s)
            color=$color_degraded;;
        10Mb/s)
            color=$color_bad;;
        Unknown!)
            color=$color_bad;;
    esac

    eth_status="^fg($color)^lm(foot echo coucou & read)󰈀 $eth_ip^lm()^fg() $separator"

fi

# connexions wifi

# color=$color_bad
# wifi_status="^fg($color)󰖩^fg()"
wifi_status=""

# connexion wlan0

wifi_ip=$(nmcli device show wlan0 | awk '/IP4.ADDRESS/ {print $2}' | cut -d/ -f 1)

if [[ -n "$wifi_ip" ]]; then
    wifi_ssid=$(nmcli connection show --active | awk '/wlan0/ {print $1}')
    wifi_link=$(cat /proc/net/wireless | awk 'NR==3 {print $3}' | head --bytes=-2)
    if [[ $wifi_link -gt 33 ]]; then
        color=$color_degraded
        if [[ $wifi_link -gt 66 ]]; then
            color=$color_good
        fi
    fi
    wifi_status=" ^fg($color)^lm(foot nmcli device wifi & read)󰖩 $wifi_link% $wifi_ssid $wifi_ip^lm()^fg() $separator"
fi

# connexion wlan1

# wifi_ip=$(nmcli device show wlan1 | awk '/IP4.ADDRESS/ {print $2}' | cut -d/ -f 1)

# if [[ -n "$wifi_ip" ]]; then
#     wifi_ssid=$(nmcli connection show --active | awk '/wlan0/ {print $1}')
#     wifi_link=$(cat /proc/net/wireless | awk 'NR==3 {print $3}' | head --bytes=-2)
#     if [[ $wifi_link -gt 33 ]]; then
#         color=$color_degraded
#         if [[ $wifi_link -gt 66 ]]; then
#             color=$color_good
#         fi
#     fi
#     wifi_status=" ^fg($color)󰖩 $wifi_link% $wifi_ssid $wifi_ip^fg() $separator"
# fi

# connexion wlx086a0a976811

# wlx_status=""
# wifi_ip=$(nmcli device show wlx086a0a976811 | awk '/IP4.ADDRESS/ {print $2}' | cut -d/ -f 1)

# if [[ -n "$wifi_ip" ]]; then
#     wlx_status=" ^c$color_good^󰖩 wifipi $wifi_ip^fg() $separator"
# fi

# vpn

vpn_status=""
color=$color_good
if [[ -e /proc/sys/net/ipv4/conf/nordlynx ]]; then
    vpn_server=$(nordvpn status | grep Hostname | sed 's/Hostname: //' | sed 's/.nordvpn.com//')
    vpn_status="^fg($color)  $vpn_server ^fg()$separator"
fi

# température cpu

temp_cpu=$(cat /sys/class/thermal/thermal_zone0/temp | awk '{printf("%d",$1/1000)}')

color=$color_gray
if [[ $temp_cpu -ge 70 ]]; then
    color=$color_degraded
    if [[ $temp_cpu -ge 80 ]]; then
        color=$color_bad
    fi
fi

temp_status="^fg($color)^lm(foot wttr) $temp_cpu°C^lm()^fg()"

# Undervoltage & throttled detected

#Flag Bits
UNDERVOLTED=0x1
THROTTLED=0x4
HAS_UNDERVOLTED=0x10000
HAS_THROTTLED=0x40000

#Get Status, extract hex
vcgencmd_status=$(vcgencmd get_throttled)
vcgencmd_status=${vcgencmd_status#*=}

undervoltage_status=""
color=$color_gray
if ((($vcgencmd_status&HAS_UNDERVOLTED)!=0)); then
    color=$color_degraded
    undervoltage_status="^fg($color) 󱐋 ^fg()"
elif ((($vcgencmd_status&UNDERVOLTED)!=0)); then
    color=$color_bad
    undervoltage_status="^fg($color) 󱐋 ^fg()"
fi

throttled_status=""
color=$color_gray
if ((($vcgencmd_status&HAS_THROTTLED)!=0)); then
    color=$color_degraded
    throttled_status="^fg($color)󰾅 ^fg()$separator"
elif ((($vcgencmd_status&THROTTLED)!=0)); then
    color=$color_bad
    throttled_status="^fg($color)󰾆 ^fg()$separator"
fi

# charge cpu

cpu_load=$(cat <(grep 'cpu ' /proc/stat) <(sleep 1 && grep 'cpu ' /proc/stat) | awk -v RS="" '{printf "%02d", ($13-$2+$15-$4)*100/($13-$2+$15-$4+$16-$5)}')
color=$color_gray
if [[ $cpu_load -ge 50 ]]; then
    color=$color_degraded
    if [[ $cpu_load -ge 75 ]]; then
        color=$color_bad
    fi
fi
cpu_status="^fg($color)^lm(foot btop) $cpu_load%^lm()^fg()"

# mémoire utilisée

mem_used=$(free -m | awk '/Mem/{print $3}')
color=$color_gray
if [[ $mem_used -ge 2000 ]]; then
    color=$color_degraded
    if [[ $mem_used -ge 3000 ]]; then
        color=$color_bad
    fi
fi
mem_status="^fg($color)^lm() $mem_used Mb^lm()^fg()"

# espace disque utilisé

# disk_used=$(df -H | awk '/dev\/nvme0n1p2/ {print $3}')
disk_used_percent=$(df | awk '/dev\/nvme0n1p2/ {print $5}' | cut -d% -f 1)
color=$color_gray
if [[ $disk_used_percent -ge 70 ]]; then
    color=$color_degraded
    if [[ $disk_used_percent -ge 85 ]]; then
        color=$color_bad
    fi
fi
disk_status="^fg($color)^lm(foot ncdu) $disk_used_percent%^lm()^fg()"

# date et heure

color=$color_gray
late_status="$(date +"%H")"
if [[ $late_status -le 6 ]]; then
    color=$color_bad
fi
if [[ $late_status -ge 22 ]]; then
    color=$color_degraded
fi
date_status="^fg($color_gray)$(date +"^lm(foot ikhal) %Y-%m-%d^lm() ^fg($color)^lm(foot tty-clock -sc) %H:%M:%S^lm()")^fg()"

# notifications

# color=$color_gray
# notification_count_status=""
# notification_count=$(swaync-client --count)
# if [[ $notification_count -ge 1 ]]; then
#     color=$color_bad
#     notification_count_status=":$notification_count"
# fi
# notification_status="^fg($color)^lm(swaync-client -t)$notification_count_status^lm()^fg()"

# window manager

wm_status="^lm(foot dwl -v && read)dl^lm()"

# sortie

echo "$keyboard_status$undervoltage_status$throttled_status$eth_status$wifi_status$vpn_status $temp_status $separator $cpu_status $separator $mem_status $separator $disk_status $separator $date_status $wm_status"
