#!/usr/bin/env sh

# Dépendances : sudo apt install pandoc bat

pandoc -s -t markdown "$1" | batcat --style=plain --force-colorization
