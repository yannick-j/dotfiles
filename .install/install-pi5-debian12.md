# Modifs a l'install

[X] nerd-font

```sh
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/DejaVuSansMono.zip
unzip DejaVuSansMono.zip
rm DejaVuSansMono.zip
sudo mv Deja* /usr/local/share/fonts
sudo fc-cache -fv
```

[X] rofi set locale
```
raspi-config
```

[X] fastfetch
```sh
sudo apt install cmake
cd .local/bin/
git clone https://github.com/fastfetch-cli/fastfetch.git
cd fastfetch
mkdir -p build
cmake ..
cmake --build . --target fastfetch --target flashfetch
```

[X] arandr
```sh
sudo apt install arandr
```

[X] chromium
```sh
sudo apt install chromium chromium-l10n
```

extensions : bitwarden, darkreader, vimium, adblock

[X] lxappearance
```sh
sudo apt install lxappearance gnome-themes-extra
```

[X] .ssh .gnupg

[X] python venv

```sh
sudo apt install python3-virtualenv
virtualenv .venv
source .venv/bin/activate.fish
pip install neovim
pip install jupytext
```

[X] neovim

```sh
sudo apt install ripgrep
sudo apt install fd-find
sudo apt install nodejs npm -y
sudo npm install -g yarn
sudo npm install -g neovim
git clone https://github.com/neovim/neovim.git
cd neovim
git checkout stable
make CMAKE_BUILD_TYPE=RelWithDebInfo
cd build
cpack -G DEB
sudo dpkg -i nvim-linux64.deb
```

- à tester : markdown-preview

[] lazygit

```sh
sudo apt install golang
sudo apt install pinentry-gnome3
git clone https://github.com/jesseduffield/lazygit.git
cd lazygit
sudo go install
```

[] ranger

```sh
sudo apt install ueberzug
sudo apt install poppler-utils
sudo apt install imagemagick
```

[] btop

```sh

```

[] pipe-viewer

```sh

```

