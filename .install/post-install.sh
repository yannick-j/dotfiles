#!/bin/bash

# script de post-installation debian 12 (bookworm)
# git clone --recurse-submodule https://gitlab.com/yannick-j/dotfiles ~/.dotfiles
# v1.0 30.07.2023
# v1.1 31.07.2023 refactoring fonctions
# v1.2 03.08.2023 ajout pipx install 'vdirsyncer[google]'

# fonctions ---------------------------------------------------------------<<<

# infos os et kernel ------------------------------------------------------<<<
_fn_os_info() {
    lsb_release -d | grep Description | cut --fields=2
    uname -srm
}
# fin infos os et kernel -------------------------------------------------->>>

# couleurs ----------------------------------------------------------------<<<

_fn_couleur() {
    case $1 in
        normal) echo -ne "\033[0m";;
        rouge) echo -ne "\033[31m";;
        vert) echo -ne "\033[32m";;
        jaune) echo -ne "\033[33m";;
        bleu) echo -ne "\033[34m";;
        magenta) echo -ne "\033[35m";;
        cyan) echo -ne "\033[36m";;
        gris) echo -ne "\033[37m";;
        hi_rouge) echo -ne "\033[91m";;
        hi_vert) echo -ne "\033[92m";;
        hi_jaune) echo -ne "\033[93m";;
        hi_bleu) echo -ne "\033[94m";;
        hi_magenta) echo -ne "\033[95m";;
        hi_cyan) echo -ne "\033[96m";;
        blanc) echo -ne "\033[97m";;
    esac
}

# fin couleurs ------------------------------------------------------------>>>

# ligne -------------------------------------------------------------------<<<
_fn_ligne() {
    local str
    str=$(printf "%0.s$2" $(seq 1 "$1")) # Fill $str with $1 characters $2
    echo "$str" # Output content of $str to terminal
}
# fin ligne --------------------------------------------------------------->>>

# titres ------------------------------------------------------------------<<<

_fn_titre() {
    # arg $1 : niveau de titre
    # arg $2 : titre
    _fn_couleur hi_bleu
    echo ''
    local n
    n=${#2} # number of characters of $2
    case $1 in
        0)
            _fn_ligne $((n+4)) '#'
            echo -n '# '
            echo -n "$2"
            echo ' #'
            _fn_ligne $((n+4)) '#';;
        1)
            _fn_ligne "$n" '='
            echo "$2"
            _fn_ligne "$n" '=';;
        2)
            _fn_ligne "$n" '-'
            echo "$2"
            _fn_ligne "$n" '-';;
        3)
            echo "$2"
            _fn_ligne "$n" '-';;
    esac
    _fn_couleur normal
}

# fin titres -------------------------------------------------------------->>>

# message -----------------------------------------------------------------<<<
_fn_message() {
    # arg $1 : message
    # arg $2 : couleur
    # arg $3 : titre
    # arg $4 : niveau de titre
    local n
    n="$4"
    n=${n:-3}
    if [ -n "$3" ]; then _fn_titre "$n" "$3"; fi
    if [ -n "$2" ]; then _fn_couleur "$2"; fi
    echo "$1"
    _fn_couleur normal
}
# fin message ------------------------------------------------------------->>>

# message d'erreur --------------------------------------------------------<<<
_fn_erreur() { _fn_message "$1" 'hi_rouge'; }
# fin message d'erreur ---------------------------------------------------->>>

# message_ok -------------------------------------------------------------<<<
_fn_message_ok() {
    _fn_message 'OK' hi_vert
}
# fin message_ok --------------------------------------------------------->>>

# oui_ou_non ? ------------------------------------------------------------<<<

_fn_oui_ou_non() {
    # arg $1 : message
    # arg $2 : choix par défaut
    local oui_non
    _fn_couleur vert
    case $2 in
        0) read -rp "$1 ? [oui/NON]: $(_fn_couleur magenta)" oui_non;;
        1) read -rp "$1 ? [OUI/non]: $(_fn_couleur magenta)" oui_non;;
    esac
    oui_non=${oui_non:-$2}
    _fn_couleur normal
    case $oui_non in
        [Nn0]*) return 1  ;;
        [OoYy1]*) return 0 ;;
    esac
}

# fin oui_ou_non ? -------------------------------------------------------->>>

# fonction test_ok --------------------------------------------------------<<<
_fn_test_ok() {
    if [ "$?" = 1 ]; then
        _fn_erreur 'Pas fait'
    else
        _fn_message_ok
    fi
}
# fin fonction test_ok ---------------------------------------------------->>>

# fonction action ---------------------------------------------------------<<<
_fn_action() {
    # arg $1 : commande
    # arg $2 : titre
    # arg $3 : niveau de titre
    local n
    n=$3
    n=${n:-2}
    if [[ -n "$2" ]]; then _fn_titre "$n" "$2"; fi
    _fn_message "$1" jaune
    _fn_oui_ou_non 'Confirmer' 0 && eval "$1"
    _fn_test_ok
}
# fin fonction action ----------------------------------------------------->>>

# fin fonctions ----------------------------------------------------------->>>

_fn_titre 0 'Post-install debian 12 bash script'
_fn_os_info

# Configuration console ---------------------------------------------------<<<

_fn_titre 1 'Configuration de la console'

_fn_message 'console-setup' jaune
grep -E 'CHARMAP|CODESET|FONTFACE|FONTSIZE' /etc/default/console-setup
_fn_message 'keyboard' jaune
grep -E 'XKBMODEL|XKBLAYOUT|XKBVARIANT|XKBOPTIONS|BACKSPACE' /etc/default/keyboard
_fn_message 'locales' jaune
echo "$LANG"
_fn_message 'timedatectl status' jaune
timedatectl status

echo ''
_fn_titre 2 'Reconfigurer la console'
_fn_message 'sudo dpkg-reconfigure console-setup locales keyboard-configuration tzdata' jaune
_fn_oui_ou_non 'Confirmer' 0 && {
    _fn_action 'sudo dpkg-reconfigure console-setup'
    grep 'CHARMAP' /etc/default/console-setup
    grep 'CODESET' /etc/default/console-setup
    grep 'FONTFACE' /etc/default/console-setup
    grep 'FONTSIZE' /etc/default/console-setup
    _fn_action 'sudo dpkg-reconfigure locales'
    echo "$LANG"
    _fn_action 'sudo dpkg-reconfigure keyboard-configuration'
    grep 'XKBMODEL' /etc/default/keyboard
    grep 'XKBLAYOUT' /etc/default/keyboard
    grep 'XKBVARIANT' /etc/default/keyboard
    grep 'XKBOPTIONS' /etc/default/keyboard
    grep 'BACKSPACE' /etc/default/keyboard
    _fn_action 'sudo dpkg-reconfigure tzdata'
    timedatectl status
}

# fin Configuration console ----------------------------------------------->>>

# Configuration réseau ----------------------------------------------------<<<

_fn_titre 1 'Configuration du réseau par network-manager'

_fn_message 'systemctl --lines=0 --no-pager status NetworkManager' jaune
systemctl --lines=0 --no-pager status NetworkManager
echo ''
_fn_message 'nmcli networking' jaune
nmcli networking
echo ''
_fn_message 'nmcli networking connectivity' jaune
nmcli networking connectivity
echo ''
_fn_message 'nmcli radio all' jaune
nmcli radio all
echo ''
_fn_message 'nmcli connection show (q pour quitter)' jaune
nmcli connection show

echo ''
_fn_message 'Modification des réglages réseau' jaune
_fn_oui_ou_non 'Confirmer' 0 && {
    _fn_action 'sudo systemctl enable NetworkManager; sudo systemctl start NetworkManager'
    systemctl --lines=0 --no-pager status NetworkManager
    _fn_action 'nmcli networking on'
    nmcli networking on
    echo -n 'nmcli networking : ' && nmcli networking
    echo -n 'nmcli networking connectivity : ' && nmcli networking connectivity
    _fn_action 'nmcli radio all on'
    nmcli radio all on
    nmcli radio all
    _fn_action 'nmcli device wifi list'
    _fn_action 'nmtui'
    _fm_message 'nmcli connection show --active' jaune
    nmcli connection show --active
}

# fin Configuration réseau ------------------------------------------------>>>

# Installation de base ----------------------------------------------------<<<

_fn_titre 1 'Installation de base'
_fn_message 'Installation des paquets de base, fichiers de config et neovim' jaune

_fn_oui_ou_non 'Confirmer' 0 && {
    _fn_action 'sudo apt update && sudo apt upgrade -y' 'Mise à jour des paquets' 2

    _fn_action 'xargs sudo apt install -y <~/.dotfiles/.install/packages-base.txt' 'Installation de packages-base.txt' 2

    _fn_message "stow --dir=~/.dotfiles */" jaune 'Installation des fichiers de config' 2
    _fn_oui_ou_non 'Confirmer' 0 && {
        if [ -f ~/.bashrc ]; then
            rm ~/.bashrc
        fi
        mkdir -p ~/.config ~/.local/bin
        basename --multiple "$(ls -d ~/.dotfiles/*/)" | xargs stow --dir="$HOME"/.dotfiles --verbose=1
        _fn_test_ok
    }

    _fn_titre 2 'Installation des plugins de neovim (et dépendances)'
    nvim --version | grep NVIM
    _fn_oui_ou_non 'Confirmer' 0 && {

        echo ''
        _fn_action 'sudo npm install -g neovim'

        _fn_action 'sudo apt install nodejs && sudo npm install -g tslib && sudo npm install -g yarn' 'dépendances de markdown-preview' 3

        _fn_action 'sudo apt install pipx && pipx install jupytext' 'dépendances de jupytext.vim' 3

        _fn_action 'curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim' 'installation de vim-plug' 3
    }

        _fn_action "nvim -c 'PlugUpgrade | PlugClean | PlugUpdate | UpdateRemotePlugins | qa'" 'Mise à jour de vim-plug et installation des plugins' 3

        _fn_action "nvim -c 'checkhealth'" "Vérifier l'état de neovim"
}

# fin installation de base ------------------------------------------------>>>

# Installation cli --------------------------------------------------------<<<

_fn_titre 1 'Installation des paquets CLI'
_fn_message 'Installation des logiciels en ligne de commande (cli)' jaune
_fn_oui_ou_non 'Confirmer' 0 && {
    _fn_action 'xargs sudo apt install -y < ~/.dotfiles/.install/packages-cli.txt' 'Installation de packages-cli.txt' 2
    _fn_action "pipx install 'vdirsyncer[google]'" 'Installation de vdirsyncer[google]' 2
}

# fin installation cli ---------------------------------------------------->>>

# Installation tty --------------------------------------------------------<<<

_fn_titre 1 'Installation des paquets TTY'
_fn_message 'Installation des logiciels pour utilisation en console : tmux et logiciels framebuffer' jaune
_fn_oui_ou_non 'Confirmer' 0 && {
    _fn_action 'xargs sudo apt install -y <~/.dotfiles/.install/packages-tty.txt' 'Installation de packages-cli.txt' 2
    mkdir -p ~/.config/tmux/plugins
    if [ ! -d ~/.config/tmux/plugins/tpm ]; then
        _fn_action 'git clone https://github.com/tmux-plugins/tpm ~/.config/tmux/plugins/tpm ' 'Installation du gestionnaire de plugins de tmux : tpm' 2
    fi
    _fn_action "$HOME/config/tmux/plugins/tpm/bin/install_plugins" 'Installation des plugins de tmux par tpm' 2
}

# fin installation tty ---------------------------------------------------->>>

# Installation wm ---------------------------------------------------------<<<

_fn_titre 1 'Installation de st & dwm'
_fn_message "Installation de l'environnement graphique" jaune
_fn_oui_ou_non 'Confirmer' 0 && {
    if [[ ! -f ~/.local/share/fonts/DejaVuSansMNerdFontMono-Regular.ttf ]]; then
        mkdir -p ~/.local/share/fonts
        _fn_action 'curl -fLo ~/.local/share/fonts/DejaVuSansMNerdFontMono-Regular.ttf --create-dirs https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/DejaVuSansMono/Regular/DejaVuSansMNerdFontMono-Regular.ttf' 'installation de DejaVu Sans Mono Nerd Font' 2
    else
        _fn_titre 2 'installation de DejaVu Sans Mono Nerd Font'
        echo 'DejaVu Sans Mono Nerd Font déja installée.'
    fi

    _fn_action 'xargs sudo apt install -y <~/.dotfiles/.install/packages-wm.txt'  'Installation de packages-wm.txt' 2

    if [[ ! -d ~/.local/bin/suckless/yst ]]; then
        mkdir -p ~/.local/bin/suckless
        _fn_action 'git clone https://gitlab.com/yannick-j/yst.git
        ~/.local/bin/suckless/yst' 'Installation de ma version de st' 2
    else
        _fn_titre 2 'Installation de ma version de st'
        echo 'st déja installé'
    fi

    if [[ -d ~/.local/bin/suckless/yst ]]; then
        cd ~/.local/bin/suckless/yst || exit 1
        _fn_action 'sudo make clean install' 'Compilation de st' 2
    else
        echo "$HOME/.local/bin/suckless/yst n'existe pas"
    fi

    if [[ ! -d ~/.local/bin/suckless/ydwm ]]; then
        mkdir -p ~/.local/bin/suckless
        _fn_action 'git clone https://gitlab.com/yannick-j/ydwm.git ~/.local/bin/suckless/ydwm' 'Installation de ma version de dwm' 2
    else
        _fn_titre 2 'Installation de ma version de dwm'
        echo 'dwm déja installé'
    fi

    if [[ -d ~/.local/bin/suckless/ydwm ]]; then
        cd ~/.local/bin/suckless/ydwm || exit 1
        _fn_action 'sudo make clean install' 'Compilation de dwm' 2
    else
        echo "$HOME/.local/bin/suckless/ydwm n'existe pas"
    fi

    _fn_action 'curl -sS https://starship.rs/install.sh | sh' 'Installation de starship' 2

    _fn_action 'python /usr/share/qutebrowser/scripts/dictcli.py install fr-FR' 'Configuration de qutebrowser' 2
    _fn_message 'run qutebrowser :adblock-update' rouge
}

# fin installation wm ----------------------------------------------------->>>

_fn_titre 0 'Fin du script'
echo ''
