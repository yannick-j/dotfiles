# fish config

if status is-interactive
    # Commands to run in interactive sessions can go here

    if [ $TERM = "xterm-256color" ] || [ $TERM = "alacritty" ] || [ $TERM = "foot" ]
        # prompt
        starship init fish | source

        # citation à l'ouverture d un terminal
        set citation (fortune anarchism)
        echo "$citation"

        # alias
        alias ls='exa --color=auto --group-directories-first --icons'
        alias la='exa -a --color=auto --group-directories-first --icons'
        alias ll='exa -l --color=auto --group-directories-first --icons'
        alias lla='exa -al --color=auto --group-directories-first --icons'
        alias vi='nvim'
        alias vim='nvim'
        alias mutt='neomutt'

        # fzf
        export FZF_DEFAULT_OPTS="-i --height=40% --color=dark"

        # bat theme
        export BAT_THEME="gruvbox-dark"

    else if [ $TERM = "linux" ]
        # curseur
        echo -e '\033[?112c'
        fastfetch

        # citation à l'ouverture d un terminal
        set citation (fortune anarchism)
        echo "$citation"
        echo ''

        # alias
        alias dwl='dwl -s "dwlb -no-ipc"'
        alias lf='lf -config ~/.config/lf/lfrc_tty'
        alias ranger='ranger --confdir ~/.config/ranger/ranger_tty'
        alias la='exa -a --color=auto --group-directories-first'
        alias ll='exa -l --color=auto --group-directories-first'
        alias lla='exa -al --color=auto --group-directories-first'
        alias vi='nvim'
        alias vim='nvim'
        alias mutt='neomutt -F ~/.config/mutt/muttrc_tty'
        alias mocp='mocp --config ~/.moc/config_tty'
        alias btop='btop -t'

        # fzf
        export FZF_DEFAULT_OPTS="-i --height=40% --color=16"

        # bat theme
        export BAT_THEME="base16"

    else if [ $TERM = "tmux-256color" ]
        # citation à l'ouverture d un terminal
        set citation (fortune anarchism)
        echo "$citation"
        echo ''

        # alias
        alias lf='lf -config ~/.config/lf/lfrc_tty'
        alias ranger='ranger -c'
        alias la='exa -a --color=auto --group-directories-first'
        alias ll='exa -l --color=auto --group-directories-first'
        alias lla='exa -al --color=auto --group-directories-first'
        alias vi='nvim'
        alias vim='nvim'
        alias mutt='neomutt -F ~/.config/mutt/muttrc_tty'
        alias btop='btop -t'

        # fzf
        export FZF_DEFAULT_OPTS="-i --height=40% --color=16"

        # bat theme
        export BAT_THEME="base16"

    end

    ### "bat" as manpager
    export MANPAGER="sh -c 'col -bx | batcat -l man -p'"

    # vi mode
    fish_vi_key_bindings

    # fzf
    source /usr/share/doc/fzf/examples/key-bindings.fish
    fzf_key_bindings
    export FZF_DEFAULT_COMMAND='find .'
    export FZF_CTRL_T_COMMAND='find .'
    # Preview file content using bat (https://github.com/sharkdp/bat)
    export FZF_CTRL_T_OPTS="
      --layout=default
      --preview 'batcat --style="changes" --color=always -r:50 {} 2>/dev/null || tree -C {}'
      --bind 'ctrl-/:change-preview-window(down|hidden|)'
      "
    # CTRL-/ to toggle small preview window to see the full command
    # CTRL-Y to copy the command into clipboard using pbcopy
    export FZF_CTRL_R_OPTS="
      --preview 'echo {}' --preview-window up:3:hidden:wrap
      --bind 'ctrl-/:toggle-preview'
      --bind 'ctrl-y:execute-silent(echo -n {2..} | pbcopy)+abort'
      --color header:italic
      --header 'Press CTRL-Y to copy command into clipboard'"
      # Print tree structure in the preview window
      export FZF_ALT_C_OPTS="--preview 'tree -C {}'"

end

# set PATH to include user's .local/bin
set PATH $PATH /home/yannick/.local/bin

# set PATH to go language
export GOPATH=/usr/local/go
set PATH $PATH $GOPATH/bin /home/yannick/.go/bin

# set qt
export QT_QPA_PLATFORMTHEME=qt5ct

# EDITOR
export EDITOR=nvim

# yazi
function y
	set tmp (mktemp -t "yazi-cwd.XXXXXX")
	yazi $argv --cwd-file="$tmp"
	if set cwd (command cat -- "$tmp"); and [ -n "$cwd" ]; and [ "$cwd" != "$PWD" ]
		builtin cd -- "$cwd"
	end
	rm -f -- "$tmp"
end
