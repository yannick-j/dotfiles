#!/bin/sh
# mise à jour des pages man
# workflow et logiciels
# dépendances : pandoc, gzip

pandoc -s workflow.md -t man -o workflow.1 -M date="`date "+%e %B %Y"`"
pandoc -s logiciels.md -t man -o logiciels.1 -M date="`date "+%e %B %Y"`"
sudo cp workflow.1 logiciels.1 /usr/share/man/man1
sudo gzip -vf /usr/share/man/man1/workflow.1
sudo gzip -vf /usr/share/man/man1/logiciels.1
sudo mandb
