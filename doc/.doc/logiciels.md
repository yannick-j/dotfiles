% LOGICIELS(1) logiciels 1.0
% Yannick

# NAME

Liste des logiciels utilisés

# SYSTÈME

- i3 (wm)
- ly (dm, https://github.com/nullgemm/ly)
- i3lock
- sct (température de couleur de l'écran)
- cfdisk (partionne les disques)

# RÉSEAU

- nmcli

# TERMINAL

- lxterminal
- kitty

# ÉDITEUR DE TEXTES

- vim

# NAVIGATEUR DE FICHIERS

- ranger
- pcmanfm

# WEB BROWSER

- firefox
- qutebrowser
- links
- w3m
- lynx
- amfora

# MAIL

- mbsync
- msmtp msmtpq msmtp-queue
- neomutt
- notmutch

# CARNETS D'ADRESSES

- vdirsyncer
- khard

# CALENDRIERS

- khal
- gcalcli

# DICTIONNAIRES

- sdcv
- aspell

# VISIONNEUR D'IMAGES

- gpicview
- feh
- display (imagemagick)
- sxiv

# ÉDITEUR D'IMAGES

- gthumb
- gimp
- inkscape

# AUDIO

- mocp
- castero

# PDF

- zathura
- evince

# EPUB

- calibre ebook-viewer
- epy

# CALCULATRICE

- orpie

# ÉLECTRONIQUE

- arduino
- arduino-cli

# Synchronisation

- rsync
- rclone

