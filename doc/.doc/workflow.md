% WORKFLOW(1) workflow 1.0
% Yannick JUINO

# NAME

workflow - Procédures courantes

# i3WM

### cmd+esc : change layout

# RÉSEAU

## Lister les réseaux wifi disponibles

	nmcli device wifi

## Se connecter à un nouveau réseau

	nmcli -a -p device wifi connect Livebox-9044 

## Se connecter à un réseau connu

	nmcli -p connection up Livebox-9044

## Lister les réseaux connus

	nmcli connection show

## Se déconnecter, reconnecter

	nmcli device wifi disconnect Livebox-9044

	nmcli device wifi connect Livebox-9044

## Éteindre ou allumer wifi et wwan

	nmcli radio all off

	nmcli radio all on

## Vpn

    nmcli -p connection up yannick

    curl ifconfig.me | xargs whois | grep netname

# MAIL

## neomutt

### changer de compte

	(F1) : gmail

	(F2) : jvcergy

### synchroniser le courrier en ligne

	(O) : lance mbsync -a

### poster en ligne

	synchroniser le courrier (terminal, mailsync) avant pour authentification

	envoyer (y) avec msmtp si réseau

### poster hors ligne, en queue avec msmtp-queue-gmail ou msmtp-queue-jvcergy

	envoyer (y)

### ajouter l'adresse électronique de l'expéditeur aux contacts

	(a) (via khard)

### compléter le champ To: avec l'adresse électronique de l'expéditeur

	(tab) (via khard)

## Terminal

### Hors-ligne

	msmtp-queue-gmail -d ou msmtp-queue-jvcergy -d pour lister les mails dans la queue

### En ligne

	msmtp-queue-gmail -r ou msmtp-queue-jvcergy -r pour envoyer les mails dans la queue

	msmtp-queue-gmail -a ou msmtp-queue-jvcergy -a pour purger les mails dans la queue

	mbsync -a pour synchroniser tous les mails

# VOLUMES

## Monter un volume usb

	sudo fdisk -l

	mount /dev/sdb2 /media/yannick -o uid=yannick,gid=yannick

    sudo mount -o rw,noatime,uid=yannick,gid=yannick /dev/sdc1 /media/yannick/usb/

## Démonter un volume

	sudo umount /media/yannick

## Monter un volume smb

	sudo mount -t cifs -o username=yannick //192.168.1.48/music /media/yannick/

## Monter un volume ntfs

    sudo mount 192.168.1.48:/volume1/homes/yannick /media/yannick/NAS

# GOOGLE DRIVE

### Lister les répertoires

	rclone lsd jvcergy:/TPs\ Pspé

### Monter le répertoire	

	rclone mount jvcergy:/"TPs Pspé/P2 Mariotte et lfsf" /media/yannick/

### Partager un fichier ou un répertoire

	rclone link jvcergy:/"TPs Pspé/P2 Mariotte et lfsf/Pspe_TP_P2_lfsf.py" 

### Répertoire partagé

    rclone lsd jvcergy:/ --drive-shared-with-me
    rclone lsd jvcergy:/"TP Tale Spé PC" --drive-shared-with-me
    rclone mount jvcergy:/"TP Tale Spé PC"/P6 /media/yannick/ --drive-shared-with-me &

# AGENDA & CONTACTS

## vdirsyncer

### Synchroniser les contacts iCloud et Google, les agendas iCloud

	vdirsyncer sync

## khard

### Rechercher dans les contacts iCloud et Google en cache et afficher le nom et l'adresse électronique

	khard <requête>

### Afficher toutes les infos sur un contact

	khard details <requête>

### Ajouter un contact

	khard new

## khal

### Consulter le calendrier et les évènements sur 2 jours

	khal calendar (ou juste khal)

### Calendrier interactif

	ikhal

### Rechercher des évènements

	khal search <requête> 

# SHELL

## history

### history : liste les dernières commandes

### !N : rappelle la Nième commande

### !! : rappelle la dernière commande

## column

### mount | column -t : met en forme la sortie de mount en colonnes.

# VIM

## Enregistrer

### :sav enregistrer sous

### : %:r nom du fichier courant sans l'extension (root):
## Plier le code

### :setlocal foldmethod=syntax (ou :setlocal fdm=syntax)
### zO : déplier

### zc : plier

### za : plier/déplier

### zr : tout déplier d'un niveau 

### zR : tout déplier

### zm : tout replier d'un niveau 

### zM : tout replier

## Changer de fenêtre

### CTRL ww

## Changer la casse

### en majuscules

	gU movement

### en minuscules

	gu movement

### basculer

	g~ movement

## Correcteur orthographique

### aspell

	<F5>

### vim built-in

	:setlocal spell spelllang=fr

	:setlocal nospell

	z= pour corriger

	zg pour ajouter au dictionnaire

	zw pour enlever du dictionnaire

# VIMTEX

## Raccourcis clavier

### Environnement

#### cse: change an environment

#### dse: delete an environment

#### tse：toggle stared environment and no-star environment, e.g., change from equation* to equation and vice versa.

#### vie: select the inside an environment

#### vae: select the entire environment (including the begin and end commands).

#### ]]: close an environment (only work in insert mode, in normal mode, it will bring cursor to next subsection instead). After you have typed begin{ENV}, use ]] to auto-close it with \end{ENV}

For more mappings, see :h vimtex-default-mappings.

## Commandes

### :VimtexInfo: show all relevant info about current LaTeX project.

### :VimtexTocOpen: show table of contents window (ESC to quit)

### :VimtexCompile: Compile the current LaTeX source file and opens the viewer after successful compilation.

### :VimtexStop: Stop compilation for the current project.

### :VimtexClean: clean auxiliary files generated in compliation process.

### :VimtexView: opend pdf

## Commentaires

### gcc : commenter/décommenter une ligne.

### gcgc : décommenter plusieurs lignes adjacentes.

### en mode V : gc pour commenter/décommenter les lignes électionnées.

# KITTY

### ctrl+shift+e : hints url

### ctrl+shift+f : hints path/filename

### ctrl+shift+o : hints open path/filename

### ctrl+shift+l : hints line

# MICRO

## Correcteur orthographique aspell

### Mise en marche

	Ctrl-e > set aspell.check on

### Suggestion

	Alt-s (Tab pour changer)

# LATEX

	vi fichier.tex

	:w !pdflatex -shell-escape fichier.tex

	:!zathura fichier.tex &

# PANDOC

## vers fichier man

    vi fichier.md

    :wq

    pandoc -s fichier.md -t man | man -l -

    pandoc -s fichier.md -t man -o fichier.1 -M date="`date "+%e %B %Y"`" 

    sudo cp fichier.1 /usr/local/man/man1

    sudo gzip /usr/local/man/man1/fichier.1

    sudo mandb


# COMPRESSION

## Décompresser un fichier

### fichier.gz

    gunzip fichier.gz

    ls

### fichier.zip

    unzip fichier.zip

    ls

### fichier.tar

    tar -xvf fichier.tar

## Compresser un fichier

### fichier.gz

    gzip fichier.gz

## Compresser les images d'un pdf

    -dPDFSETTINGS=/[screen/ebook/printer/prepress/default]

- /screen selects low-resolution output similar to the Acrobat Distiller "Screen Optimized" setting.
- /ebook selects medium-resolution output similar to the Acrobat Distiller "eBook" setting.
- /printer selects output similar to the Acrobat Distiller "Print Optimized" setting.
- /prepress selects output similar to Acrobat Distiller "Prepress Optimized" setting.
- /default selects output intended to be useful across a wide variety of uses, possibly at the expense of a larger output file.

    gs -sDEVICE=pdfwrite -dPDFSETTINGS=/printer -q -o lumiere-matiere-low.pdf lumiere-matiere.pdf

# GPG

## chiffrer un fichier pour destinataire

    gpg -r destinataire -e fichier

## déchiffre un fichier

    gpg -d fichier

## lister les clés publiques

    gpg -k

## lister les clés secrètes

    gpg -K

## chiffrer un mot de passe sans historique (vers stdin)

    gpg -e -o fichier.gpg -r yannick -

    (le - final indique vers stdin)

    taper le mot de passe

    ENTER CTRL-d

# RSYNC

## synchroniser de source vers destination

    rsync -avzh /home/yannick/Documents /media/yannick/usb/

    a, --archive archive mode; The files are transferred in "archive" mode,
    which ensures that symbolic  links,  devices,  attributes, permissions,
    ownerships, etc. are preserved in the transfer.

    -v, --verbose; increase verbosity

    z, --compress; compress file data during the transfer

    h, --human-readable; output numbers in a human-readable format

    -delete; delete extraneous files from dest dirs

    -P; same as --partial --progress

# GIT

    git status

    git add -A

    git status

    git commit -m "message"

    git status

    git push

    git status

# Paquets

## Liste des paquets installés

    sudo dpkg-query -f '${binary:Package}\n' -W > packages_list.txt

## Installer les paquets listés

    sudo xargs -a packages_list.txt apt install

