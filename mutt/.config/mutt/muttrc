# vim: filetype=neomuttrc
# fichier de configuration de neomutt
# version 0.1 19/08/2020
# https://neomutt.org/guide/configuration
# https://wiki.archlinux.org/index.php/Mutt

# 1.1. Location of system config files
# /etc/neomuttrc
# 1.2. Location of user config files
# ~/.config/mutt/muttrc

# couleurs
source ~/.config/mutt/colors.muttrc

# charge les réglages du compte par défaut
source ~/.config/mutt/accounts/1-gmail.muttrc

# macros changement de compte
macro index,pager <f5> '<enter-command>source ~/.config/mutt/accounts/1-gmail.muttrc<enter>\
<change-folder>!<enter><sync-mailbox>;' "ouvrir le compte yannick.juino@gmail.com"
macro index,pager <f6> '<enter-command>source ~/.config/mutt/accounts/2-jvcergy.muttrc<enter>\
<change-folder>!<enter><sync-mailbox>;' "ouvrir le compte yannick.juino@jvcergy.com"

# macro pour relever du courrier
# macro index,pager O "\
# <enter-command>unset wait_key<enter>\
# <shell-escape>i3-sensible-terminal --hold --detach --class mailsync --title mailsync -e mailsync<enter>\
# <enter-command>echo 'Comptes synchronisés' set wait_key<enter>"\
#  "synchronise les comptes"

# réglages généraux
# editeur de texte
set editor="nvim"
# set editor="vi + -c 'startinsert'"
# mailcap et tmp
#set tmpdir = ~/.mutt/tmp
set mailcap_path = ~/.config/mutt/mailcap
# scrolling
set menu_scroll
set menu_context=6
# date
set date_format = "%d/%m/%y %H:%M%p"
# présentation de l'index
set index_format="%5C %Z %?X?A& ? %D %-15.15F %s (%X/%c)"
set sort = reverse-date-received

# quitter
set quit = ask-yes
# visualiser html en texte
auto_view text/html
# visualiser csv en texte
auto_view text/csv
# visualiser docx en texte
auto_view application/vnd.openxmlformats-officedocument.wordprocessingml.document 
# visualiser xls en texte
auto_view application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
# visualiser odt en texte
auto_view application/vnd.oasis.opendocument.text
# visualiser pdf en texte
auto_view application/pdf
auto_view application/x-pdf
auto_view text/pdf
auto_view text/x-pdf

# Order to try and show multipart emails
alternative_order text/plain text/enriched text/html
# enlever les '+' sur les lignes coupées
set markers = no
# messages anciens
set mark_old
# sidebar
set mail_check_stats
set sidebar_visible = no
set sidebar_short_path = yes
set sidebar_width = 14
set sidebar_format = '%B%> %?N?%N?' 
bind index,pager B sidebar-toggle-visible
# barre d'aide
# unset help
# editeur et pager
# En-tête dans l'éditeur
set edit_headers
# saute la demande de destinataire (To:) et sujet
#set autoedit
set pager_context = 3
# stoppe en fin de message au lieu de passer au suivant
set pager_stop
# Display the index above the pager view
set pager_index_lines=8
# set status_on_top
set abort_noattach = ask-yes
set abort_noattach_regex = "\\<pj|pièce jointe|pièces jointes\\>"

# demande champ CC:
set askcc
# demande champ BCC:
set askbcc
# demande si forward avec attach
# set forward_attachments = ask-no
set mime_forward = ask-no

# query addressbook
## khard
set query_command = "khard email --parsable '%s'"
bind editor <Tab> complete-query
macro index,pager A "<pipe-message>khard add-email<return>" \
"ajouter aux contacts khard"
## abook
# set query_command= "abook --mutt-query '%s'"
# macro index,pager  A "<pipe-message>abook --add-email-quiet<return>" "Add this sender to Abook"
# bind editor        <Tab> complete-query

# urlview or urlscan
# macro index,pager \cb |urlscan\n "urlscan"
macro index,pager \cb "<pipe-message> urlscan<Enter>" "call urlscan to extract URLs out of a message"
macro attach,compose \cb "<pipe-entry> urlscan<Enter>" "call urlscan to extract URLs out of a message"

# vkhal fichiers ics
#macro attach K "<enter-command>unset wait_key<enter><shell-escape>rm -f /tmp/events.ics<enter><save-entry><kill-line>/tmp/events.ics<enter><shell-escape>vkhal -k /tmp/events.ics<enter>"

# notmuch indexe les mails
# All the notmuch settings are documented here: https://neomutt.org/feature/notmuch
macro index <F8> \
"<enter-command>set my_old_pipe_decode=\$pipe_decode my_old_wait_key=\$wait_key nopipe_decode nowait_key<enter>\
<shell-escape>notmuch-mutt -r --prompt search<enter>\
<change-folder-readonly>`echo ${XDG_CACHE_HOME:-$HOME/.cache}/notmuch/mutt/results`<enter>\
<enter-command>set pipe_decode=\$my_old_pipe_decode wait_key=\$my_old_wait_key<enter>" \
 "notmuch: search mail"
# Points to the notmuch directory
#set nm_default_url = "notmuch://$HOME/.mail/work"
# Makes notmuch return threads rather than messages
#set nm_query_type = "threads"
# Binding for notmuch search
bind index \\ vfolder-from-query

# correcteur orthographique
#set ispell="aspell -a"
macro compose j  "<filter-entry>newsbody -hqs -n - -p aspell check %f<enter>"  "aspell"

# gpg
source ~/.config/mutt/gpg.muttrc

# powerline
source ~/.config/mutt/powerline.neomuttrc

# keybindings
source ~/.config/mutt/keybindings.muttrc

